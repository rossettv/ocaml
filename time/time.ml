(** Time and timezones management *)

(** [user f x] runs the command [f x] and evaluates the real time 
    (apparent time) of its execution. It prints the result in seconds
    and returns the same output as [f x]. *)
let real f x = 
     let t0 = Unix.gettimeofday () 
  in let res=f x 
  in let t1 = Unix.gettimeofday () 
  in let () = Printf.printf "real time: %f s\n%!" (t1 -. t0)
  in res

(* The following functions are adapted from the 
   source code of Ivan Gotovchits
   https://github.com/ivg/date-time/blob/master/dateTime.ml
   https://stackoverflow.com/questions/44008058/handle-date-and-time-in-ocaml
*)

(** [zone] is a time zone *)
type tz = Local | UTC | UTC_dec of int

(** The type [Time.t] is a date-time object. *)
type t = 
| Bundle of Unix.tm
| Seconds of float

(**/**)
(** [to_seconds t] converts [t] into [Seconds] format. Internal usage only *)
let to_seconds = function 
| Bundle tm -> Seconds (fst (Unix.mktime tm))
| s -> s 

(** [to_bundle ~tz t] converts [t] into [Bundle] format. Internal usage only. *)
let rec to_bundle ?(tz=Local) = function 
| Seconds s -> begin match tz with 
  | Local -> Unix.localtime s
  | UTC   -> Unix.gmtime s
  | UTC_dec n -> let m = Unix.gmtime s in 
     to_bundle (Bundle {m with tm_hour=m.tm_hour+n})
  end
| Bundle tm -> snd (Unix.mktime tm)
(**/**)

(** [to_float t] converts [t] into the number of seconds since the 1st January 1970,
    start of the so-called digital era. *)
let to_float time = 
  match to_seconds time with Seconds s -> s | _ -> failwith "should never happen"

(** [utc_local_offset t] returns the numbers of hours between the local time
    and the UTC. *)
let utc_local_offset time =
  let t = to_float time in
  let loc, utc = Unix.localtime t, Unix.gmtime t in
  let off = loc.Unix.tm_hour - utc.Unix.tm_hour in
  if off < -12 then off + 24 else off

(** [full_weekday n] returns the name of the weekday (0 is Monday) in French. *)
let full_weekday = function
| 0 -> "lundi"
| 1 -> "mardi"
| 2 -> "mercredi"
| 3 -> "jeudi"
| 4 -> "vendredi"
| 5 -> "samedi"
| 6 -> "dimanche"
| _ -> failwith "full_weekday"

(** [short_weekday n] returns the three letter short name 
    of the weekday (0 is Monday) in French. *)
let short_weekday = function 
| 0 -> "lun"
| 1 -> "mar"
| 2 -> "mer"
| 3 -> "jeu"
| 4 -> "ven"
| 5 -> "sam"
| 6 -> "dim"
| _ -> failwith "short_weekday"

(** [full_month n] returns the name of the month (0 is January) in French. *)
let full_month = function 
| 0 -> "janvier"
| 1 -> "février"
| 2 -> "mars"
| 3 -> "avril"
| 4 -> "mai"
| 5 -> "juin"
| 6 -> "juillet"
| 7 -> "août"
| 8 -> "septembre"
| 9 -> "octobre"
| 10 -> "novembre"
| 11 -> "décembre"
| _ -> failwith "full_month"

(** [short_month n] returns the three letter short name of the month 
    (0 is January) in French. *)
let short_month = function 
| 0 -> "jan"
| 1 -> "fév"
| 2 -> "mar"
| 3 -> "avr"
| 4 -> "mai"
| 5 -> "jun"
| 6 -> "jul"
| 7 -> "aoû"
| 8 -> "sep"
| 9 -> "oct"
| 10 -> "nov"
| 11 -> "déc"
| _ -> failwith "short_month"

(** [of_local year month day hour minute second] return a [Time.t] object
    coding for the local time given as argument. *)
let of_local y mon d h m s =
  let f,s = modf s in
  let tm = {
    Unix.tm_sec = int_of_float s;
    Unix.tm_min = m;
    Unix.tm_hour = h;
    Unix.tm_mday = d;
    Unix.tm_mon = mon - 1;
    Unix.tm_year = y - 1900;
    (* The following values are not correct at this point, 
       they will be determined by the call to Unix.mktime. *)
    Unix.tm_wday = 0;
    Unix.tm_yday = 0;
    Unix.tm_isdst = false;
  } in
  let r,tm = Unix.mktime tm in
  Seconds (r +. f)

(** [of_utc year month day hour minute second] is the same as [of_local]
    but returns UTC time. *)
let of_utc y mon d h m s =
  let t = of_local y mon d h m s in
  of_local y mon d (h + utc_local_offset t) m s

(** [of_utc_dec offset year month day hour minute second] is the same as [of_utc]
    but returns UTC+offset time. *)
let of_utc_dec off y mon d h m s = of_utc y mon d (h-off) m s

(** [create tz year month day hour minute second] 
    uses one of [of_local], [of_utc] or [of_utc_dec] according to [zone]. *)
let create zone year month day hour min sec = 
  let f = match zone with
    | Local -> of_local
    | UTC   -> of_utc
    | UTC_dec n -> of_utc_dec n in
  f year month day hour min sec

let make ?(tz=Local) ~year ~month ~day
    ?(hour=0) ?(min=0) ?(sec=0.) () =
  create tz year month day hour min sec

(** [now ()] returns a [Time.t] object for the current local time. *)
let now () = Seconds (Unix.gettimeofday ())

(** [strftime ~tz fmt t] returns a formatted string 
    where the following atoms are replaced : 
 -   [%a]  short name of the day of the week
 -   [%A]  full name of the day of the week
 -   [%b]  short name of the month
 -   [%B]  full name of the month
 -   [%C]  century 
 -   [%d]  day of the month
 -   [%D]  date (format [dd/mm/yy])
 -   [%e]  month number
 -   [%F]  date (format [yyyy-mm-dd])
 -   [%g]  century year (two digits)
 -   [%h]  short name of the month (same as [%b])
 -   [%H]  hour in 24 hour format
 -   [%I]  hour in 12 hour format
 -   [%j]  day of the year
 -   [%m]  month of the year
 -   [%M]  minutes
 -   [%N]  nanoseconds 
 -   [%n]  end of line "\n"
 -   [%p]  "AM" or "PM"
 -   [%P]  "am" or "pm"
 -   [%q]  quarter of the year
 -   [%r]  12-hour ([hh:mm:ss am/pm])
 -   [%R]  24-hour ([hh:mm])
 -   [%S]  seconds of the minute
 -   [%s]  seconds since [1970-01-01 00:00:00] 
 -   [%t]  tabulation "\t" 
 -   [%T]  24-hour ([hh:mm:ss])
 -   [%u]  day of the week (Monday is 1, Sunday is 7)
 -   [%w]  day of the week (Sunday is 1, Monday is 2)
 -   [%Y]  year
 -   [%y]  century year (two digits) 

*)
let rec strftime ?(tz=Local) fmt t =
  let fmt2= Str.global_replace (Str.regexp "[%[^[%]") "$" fmt in
  let f,s = modf t in
  let tm = to_bundle ~tz (Seconds s) in
  let b = Buffer.create 32 in
  let sub = function
    | "a" -> short_weekday tm.tm_wday
    | "A" -> full_weekday tm.tm_wday
    | "b" | "h" -> short_month tm.tm_mon
    | "B" -> full_month tm.tm_mon
    | "C" -> Printf.sprintf "%02d" (tm.tm_year / 100 + 19)
    | "d" -> Printf.sprintf "%02d" tm.tm_mday
    | "D" -> strftime ~tz "%d/%m%y" t
    | "e" -> Printf.sprintf "% 2d" (tm.tm_mon + 1)
    | "F" -> strftime ~tz "%+4Y-%m-%j" t
    | "g" -> Printf.sprintf "%02d" (tm.tm_year mod 100) 
    | "H" -> Printf.sprintf "%02d" tm.tm_hour
    | "I" -> Printf.sprintf "%02d" ((tm.tm_hour-1) mod 12 + 1)
    | "j" -> Printf.sprintf "%03d" tm.tm_yday
    | "Y" -> Printf.sprintf "%d" (tm.tm_year + 1900)
    | "y" -> Printf.sprintf "%02d" (tm.tm_year mod 100)
    | "m" -> Printf.sprintf "%02d" (tm.tm_mon + 1)
    | "M" -> Printf.sprintf "%02d" tm.tm_min
    | "N" -> Printf.sprintf "%09d" (int_of_float (f *. 1e9))
    | "n" -> "\n"
    | "p" -> if tm.tm_hour > 11 then "PM" else "AM"
    | "P" -> if tm.tm_hour > 11 then "pm" else "am"
    | "q" -> Printf.sprintf "%d" (tm.tm_mon / 4 +1)
    | "r" -> strftime ~tz "%I:%M:%S %p" t
    | "R" -> strftime ~tz "%H:%M" t
    | "S" -> Printf.sprintf "%02d" tm.tm_sec
    | "s" -> Printf.sprintf "%d" (int_of_float t)
    | "t" -> "\t" 
    | "T" -> strftime ~tz "%H:%M:%S" t
    | "u" -> Printf.sprintf "%d" (tm.tm_wday + 1)
    | "w" -> Printf.sprintf "%d" ((tm.tm_wday + 1) mod 7)
    | "ms" -> Printf.sprintf "%03d" (int_of_float (f *. 1e3))
    | "mS" -> Printf.sprintf "%03.7g" (f *. 1e3)
    | s    -> invalid_arg ("strftime: unknown variable %" ^ s) in
  Buffer.add_substitute b sub fmt2;
  Buffer.contents b

(** [of_string s] attempts to convert the string into a [Time.t] object. *)
let of_string ?(tz=Local) str =
  Scanf.sscanf str "%d%_c%d%_c%d%_c%d%_c%d%_c%f"
    begin match tz with
      | Local -> of_local
      | UTC -> of_utc
      | UTC_dec n -> of_utc_dec n
    end

(** [of_string_opt ~tz s] same as {!of_string} but returns a [Time.t option]. *)
let of_string_opt ?(tz=Local) str =
  begin try Some (of_string ~tz str) with _ -> None end

(** [seconds_of_string ~tz s] attemps to convert the string into 
    a number of seconds since 1970-01-01 00:00:00. *)
let seconds_of_string ?(tz=Local) str = 
  of_string ~tz str |> to_float

(** [of_string_opt ~tz s] same as {!of_string} but returns a [float option]. *)
let seconds_of_string_opt ?(tz=Local) str =
  begin try Some (seconds_of_string ~tz str) with _ -> None end

(** [strfnow ~tz fmt ()] is the same as [strftime ~tz fmt (now ())]. *) 
let strfnow ?(tz=Local) fmt ()= Unix.gettimeofday () |> strftime ~tz fmt

class time = 
  let tz = ref Local
  and t = ref (now () |> to_float) 
  in object
    method set s = t := of_string ~tz:(!tz) s |> to_float
    method utc n = tz := if n=0 then UTC else UTC_dec n
    method strf fmt = strftime ~tz:(!tz) fmt !t
  end
