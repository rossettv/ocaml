(** {1 Statistical tools for multiple purposes *)

(** {2 Distribution of numbers} *)

type float = { mean : Float.t; std : Float.t} 

(** [add a b] returns the sum of two numbers *)
let add a b = 
  { mean = a.mean +. b.mean; 
    std = sqrt (a.std *. a.std +. b.std *. b.std) } 

(** [sub a b] returns the difference [a-b]. *)
let sub a b = 
  { mean = a.mean -. b.mean; 
    std = sqrt (a.std *. a.std +. b.std *. b.std) }

(** [compare ~c a b] returns [0] if the confience intervals of [a] and [b] overlap.
    The confidence intervals are defined by the coefficient [c]. By default [c] is equal
    to 1.96, which corresponds to the 95 % confidence interval. 
    If the confidence intervals do not overlap, [compare ~c a b] returns [-1] if the
    confidence interval of [a] is lower than the confidence interval of [b] and 
    it returns [1] in the opposite case. *)
let compare ?(c : Float.t =1.96) a b = 
  if a.mean +. c *. a.std < b.mean -. c *. b.std 
  then -1
  else if b.mean +. c *. b.std < a.mean -. c *. a.std
  then 1
  else 0

(** [of list l] returns the statistical [float] computed from the values in [l]. *) 
let of_list (l : Float.t list) = 
  let n = ref 0. 
  and s = ref 0.
  and q = ref 0. in 
  List.iter (fun x -> 
    n := !n +. 1. ;
    s := !s +. x ;
    q := !q +. x) l;
  let mean = !s /. !n in
  { mean;
    std = sqrt (!q /. !n -. mean *. mean) }

(** [of_array a]  returns the statistical [float] computed from the values in [a]. *)
let of_array a = of_list (Array.to_list a)
