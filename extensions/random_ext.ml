(** {2 [Random] extensions} *)
(** Uniformly distributed random number between 0 and 1.
    Mathematical notation {%latex:$\mathcal U\big(\,[0,1]\,\big)$%}. *)
let uniform () = Random.float 1.

(** [bernoulli p] is [true] with probability [p] and [false] otherwise.
    Mathematical notation {%latex:$\mathcal B(p)$%}. *)
let bernoulli p = uniform () < p
(** [bool p] is an alias for [bernoulli p] with extra tests for the value [p]. *)
let bool = function 
| x when x <= 0.0 -> false
| x when x >= 1.0 -> true
| p -> bernoulli p 

(** [round x] returns either [ceil x] of [floor x] such that 
    the statistical average of [round x] is equal to [x]. We can write
    [round x] as a process 
    {%latex:$\mathcal R(x)=\lfloor x\rfloor+\mathcal B(\\{x\\})$%},
    {%latex:$\\{x\\}$%} is the fractional part of {%latex:$x$%} : 
    {%latex:$\\{x\\}=x-\lfloor x\rfloor$%}. *)
let round x =
  let frac, n = modf x in
  n +. (if bernoulli frac then 1. else 0.)

(** When [a] is an array of positive real numbers, [proportional ~sum a] is
    a randomly chosen index [i] with probability proportional
    to the associated value [a.(i)]. If the value [sum] is provided,
    the sum of coefficients is not computed, the value of [sum] is used
    instead. 
   
    This function is useful for tabulated probability distributions. *)
let proportional ?(sum=0.) p =
  let s = 
    if sum=0. 
    then Array.fold_left (+.) 0. p 
    else sum 
  in
  let x = Random.float s 
  and i = ref 0
  and c = ref p.(0) in
  while x > !c do
    incr i;
    c := !c +. p.(!i)
  done;
  !i

(** [exp b] is an exponentially distributed positive real number 
    with average [b]. Mathematical notation {%latex:$\mathcal E(b)$%}.*)
let exp x = -. Stdlib.log (uniform ()) *. x

(** [trunc_exp b a] is a random float number distributed between 0 and {%latex:$a$%} 
    and is exponential. Its density 
    is given by {%latex:$p(x)=\frac{1}{b}\frac{\exp(-x/b)}{1-\exp(-a/b)}$%}.
    Its average value is {%latex:$b-(a+b)\exp(-a/b)$%}. *)
let trunc_exp b a =
  let c = 1. -. Stdlib.exp (-. a /. b) in
  -. Stdlib.log (1. -. c *. uniform () ) *. b

(** [min_exp a] is the min of numbers exponentially distributed 
    with the coefficients [a] as parameters. It returns the
    minimum value and the index to which it corresponds. As it is
    equivalent to [(Random.exp s, proportional a)] ([s] is the sum of the
    elements of [a]) this procedure only uses two random calls. *)
let min_exp a =
  let sum = Array.fold_left (+.) 0. a in
  (exp sum, proportional ~sum a)

(** [normal ()] is a Gaussian distributed random number 
    distributed according to {%latex:$\mathcal{N}(0,\,1)$%}
    with mean 0 and variance 1.  
    This implementation uses the Marsaglia variant of the 
    {%latex:Box-Muller%} algorithm with memoization. *)
let normal =
  let memo : float option ref = ref None in
  let self () = begin 
  match !memo with 
  | None -> 
    let u=ref 0.
    and v=ref 0.
    and s=ref 2.
    in while !s > 1. do
      u := uniform () *. 2. -. 1.;
      v := uniform () *. 2. -. 1.;
      s := !u *. !u +. !v *. !v
    done;
    let r = sqrt(-. (log !s) *. 2. /. !s) in 
    memo := Some (r *. !u);
    r *. !v
  | Some x -> 
      memo := None;
      x
  end in
  self

(** {2 Advanced features} *)
(** Returns a random permutation from {%latex:$\mathcal{S}_n$%}.
  This is Durstenfeld's in-place version of
  the Fisher-Yates shuffle algorithm, also called Knuth shuffle. 
  {%latex:from \textit{Algorithm 235: Random permutation} by 
  Durstenfeld, R. \textit{Communications of the ACM} \textbf{7} (7),
  page 420 (1964)%}. *) 
let fisher_yates n =
  let a = Array.init n Fun.id in
  for k = n-1 downto 1 do
    let i = Random.int (k+1) in  (* 0 <= i <= k *)
    let x = a.(i) in
    a.(i) <- a.(k);
    a.(k) <- x
  done;
  a

(* Mike Giles, Approximating the erfinv function,
  doi = {10.1016/b978-0-12-385963-1.00010-1},
  year  = {2012},
  publisher = {Elsevier {BV}},
  pages = {109--116},
  author = {Mike Giles},
  title = {Approximating the erfinv Function},
  booktitle = {{GPU} Computing Gems Jade Edition}
}*)
(** [inverf a] is a random float distributed according
    to the inverse error function. *)
let inverf x=
  let w=ref (-. log (1. -. x *. x))
  and p = ref 2.81022636e-08 in
  if !w < 0.5 then begin
    w := !w -. 2.5;
    p := 3.43273939e-07 +. !p *. !w;
    p := -3.5233877e-06 +. !p *. !w;
    p := -4.39150654e-06 +. !p *. !w;
    p := 0.00021858087 +. !p *. !w;
    p := -0.00125372503 +. !p *. !w;
    p := -0.00417768164 +. !p *. !w;
    p := 0.246640727 +. !p *. !w;
    p := 1.50140941 +. !p *. !w
  end else begin
    w := sqrt !w -. 3.;
    p := -0.000200214257;
    p := 0.000100950558 +. !p *. !w;
    p := 0.00134934322 +. !p *. !w;
    p := -0.00367342844 +. !p *. !w;
    p := 0.00573950773 +. !p *. !w;
    p := -0.0076224613 +. !p *. !w;
    p := 0.00943887047 +. !p *. !w;
    p := 1.00167406 +. !p*. !w;
    p := 2.83297682 +. !p*. !w
  end;
  !p *. x

(** [smirnov diffusivity a] returns a random number [t] distributed 
    according to Smirnov's law, that corresponds to the first-passage time 
    distribution at origin of a Brownian motion with given [diffusivity]
    starting at distance [a]. 
   
    {%latex:$$p(t) = \frac{a}{\sqrt{4\pi D t^3}} \mathrm{e}^{-\tfrac{a^2}{4Dt}} $$%}

    This implementation uses the inverse function [inverf] to implement
    a standard inverse of cumulative function mathod, where the cumulative
    function is {%latex:$F(t)=\mathrm{erfc}\big(\frac{a}{\sqrt{2Dt}}\big)$%}.
*)
    
(* F(t) = erfc(a/sqrt(2 D t)) 
   G(u) = a^2/(2 D) 1/inverf(1-u)^2 *)
let smirnov d a =
  let y = uniform () |> inverf in 
  let q = a /. (min y 5.805018683193454) in
  0.5 *. q *. q /. d

(** [poisson lambda] returns a random integer from 0 to infinity 
    distributed according to Poisson's law of parameter [lambda]. *)
let poisson lambda =
  let x=exp (-. lambda) 
  and k=ref (-1) 
  and p=ref 1. in

  while !p > x do
    incr k;
    p := uniform () *. !p
  done;
  !k
