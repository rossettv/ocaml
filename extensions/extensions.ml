(** Extensions of the standard library *)
(**/**)

include Stdlib_ext

module Const = struct 
  include Const_ext
end

module Int = struct
  include Int
  type t = int
  include Int_ext
end

module Char = struct 
  include Char
  type t = char
  include Char_ext
end

(* Extensions on module Float create signature issues with fpclass, among others *)

module String = struct 
  include String
  type t = string
  include String_ext
end
  
module Array = struct 
  include Array
  type 'a t = 'a array
  include Array_ext
end 

module List = struct 
  include List
  type 'a t = 'a list
  include List_ext
end

module Filename = struct 
  include Filename
  type t = string
  include Filename_ext
end

module Complex = struct 
  include Complex
  include Complex_ext
end

module Stack = struct 
  include Stack
  include Stack_ext
end

module Queue = struct 
  include Queue
  include Queue_ext
end

module Str = struct 
  include Str
  type regexp = Str.regexp 
  include Str_ext
end

(* module Arg = struct 
  include Arg
  include Arg_ext
end *)

module Random = struct
  include Random
  include Random_ext
end
