(** Ordered sets. 

    This implementation is adapted from the [Set]
    from the Objective Caml standard library
    by Xavier Leroy.
*)

let string_shift s =
  "       "^(Str.split (Str.regexp "\n") s |> String.concat "\n      ")

(* Sets over ordered types *)
module type OrderedType =
  sig
    type t
    val compare: t -> t -> int
    val to_string: t -> string 
  end

module type T = sig
  type elt
  type t
  val empty: t
  val is_empty: t -> bool
  val cardinal: t -> int
  val depth : t -> int
  val mem: elt -> t -> bool
  val add: elt -> t -> t
  val singleton: elt -> t
  val remove: elt -> t -> t
  val union: t -> t -> t
  val inter: t -> t -> t
  val disjoint: t -> t -> bool
  val diff: t -> t -> t
  val compare: t -> t -> int
  val equal: t -> t -> bool
  val subset: t -> t -> bool
  val iter: (elt -> unit) -> t -> unit
  val map: (elt -> elt) -> t -> t
  val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val for_all: (elt -> bool) -> t -> bool
  val exists: (elt -> bool) -> t -> bool
  val filter: (elt -> bool) -> t -> t
  val filter_map: (elt -> elt option) -> t -> t
  val partition: (elt -> bool) -> t -> t * t
  val elts: t -> elt list
  val min: t -> elt
  val min_opt: t -> elt option
  val max: t -> elt
  val max_opt: t -> elt option
  val choose: t -> elt
  val choose_opt: t -> elt option
  val nth: t -> int -> elt
  val rand: t -> elt
  val split: elt -> t -> t * bool * t
  val find: elt -> t -> elt
  val find_opt: elt -> t -> elt option
  val find_first: (elt -> bool) -> t -> elt
  val find_first_opt: (elt -> bool) -> t -> elt option
  val find_last: (elt -> bool) -> t -> elt
  val find_last_opt: (elt -> bool) -> t -> elt option
  val of_list: elt list -> t
  val to_seq_from : elt -> t -> elt Seq.t
  val to_seq : t -> elt Seq.t
  val to_rev_seq : t -> elt Seq.t
  val add_seq : elt Seq.t -> t -> t
  val of_seq : elt Seq.t -> t
end

module Make(X: OrderedType) : T with type elt = X.t = struct
  type elt = X.t
  type t = Empty 
  | Node of {
    left:  t; 
    value: elt; 
    right: t; 
    height:int; 
    size  :int
  }

  let singleton x = 
    Node{left=Empty; value=x; right=Empty; height=1; size=1}

  (* Sets are represented by balanced binary trees (the heights of the
     children differ by at most 2. The size w indicates
     the number of elts in the tree (and sub trees). *)
  let depth = function
  | Empty -> 0
  | Node {height} -> height

  let cardinal = function 
  | Empty -> 0
  | Node {size} -> size

  let rec iter (f : elt -> unit) = function
  | Empty -> ()
  | Node {left; value; right} -> iter f left; f value; iter f right

  let rec mem (x:elt) = function
  | Empty -> false
  | Node {left; value; right} ->
    let c = X.compare x value in
    c = 0 || mem x (if c < 0 then left else right)

  (* Creates a new node with left son l, value v and right son r.
     We must have all elts of l < v < all elts of r.
     l and r must be balanced and | height l - height r | <= 2.
     Inline expansion of height for better speed. *)
  let create_ob2 left value right =
    let hl = match left  with Empty -> 0 | Node {height} -> height in
    let hr = match right with Empty -> 0 | Node {height} -> height in
    let wl = match left  with Empty -> 0 | Node {size} -> size in
    let wr = match right with Empty -> 0 | Node {size} -> size in
    Node {left; value; right; 
          height = (if hl >= hr then hl + 1 else hr + 1);
          size = wl + 1 + wr}

  (* Same as create_ob2, but performs one step of rebalancing if necessary.
     Assumes l and r balanced and | height l - height r | <= 3.
     Inline expansion of create_ob2 for better speed in the most frequent case
     where no rebalancing is required. *)
  let create_ob3 l v r =
    let hl = match l with Empty -> 0 | Node {height} -> height in
    let hr = match r with Empty -> 0 | Node {height} -> height in
    if hl > hr + 2 then begin match l with
    | Empty -> invalid_arg "Set.create_ob3"
    | Node {left=ll; value=lv; right=lr} ->
      if depth ll >= depth lr
      then create_ob2 ll lv (create_ob2 lr v r)
      else begin match lr with
      | Empty -> invalid_arg "Set.create_ob3"
      | Node {left=lrl; value=lrv; right=lrr} ->
         create_ob2 (create_ob2 ll lv lrl) lrv (create_ob2 lrr v r)
      end
    end else if hr > hl + 2 then begin match r with
    | Empty -> invalid_arg "Set.create_ob3"
    | Node {left=rl; value=rv; right=rr} ->
      if depth rr >= depth rl 
      then create_ob2 (create_ob2 l v rl) rv rr
      else begin match rl with
      | Empty -> invalid_arg "Set.create_ob3"
      | Node {left=rll; value=rlv; right=rlr} ->
         create_ob2 (create_ob2 l v rll) rlv (create_ob2 rlr rv rr)
      end
    end else
      Node{left=l; value=v; right=r; 
      height = (if hl >= hr then hl + 1 else hr + 1);
      size = cardinal l + 1 + cardinal r}

  (* Insertion of one elt *)
  let rec add x = function
  | Empty -> singleton x 
  | Node {left; value; right } as t ->
    let c = X.compare x value in
    if c = 0 then t else
      if c < 0 then
        let ll = add x left in
        if left == ll then t else create_ob3 ll value right
      else
        let rr = add x right in
        if right == rr then t else create_ob3 left value rr

  (* Beware: those two functions assume that the added v is *strictly*
     smaller (or bigger) than all the present elts in the tree; it
     does not test for equality with the current min (or max) elt.
     Indeed, they are only used during the "create_ob" operation which
     respects this precondition.
  *)
  let rec add_min_elt x = function
  | Empty -> singleton x
  | Node {left; value; right} ->
    create_ob3 (add_min_elt x left) value right

  let rec add_max_elt x = function
  | Empty -> singleton x
  | Node {left; value; right} ->
    create_ob3 left value (add_max_elt x right)

  (* Same as create_ob2 and create_ob3, but no assumptions are made on the
     relative heights of l and r. *)
  let rec create_ob l v r = match (l, r) with
  | (Empty, _) -> add_min_elt v r
  | (_, Empty) -> add_max_elt v l
  | (Node{left=ll; value=lv; right=lr; height=lh}, 
     Node{left=rl; value=rv; right=rr; height=rh}) ->
      if lh > rh + 2 
      then create_ob3 ll lv (create_ob lr v r) 
      else if rh > lh + 2 
      then create_ob3 (create_ob l v rl) rv rr 
      else create_ob2 l v r

  (* Smallest and greatest elt of a set *)
  let rec min = function
  | Empty -> raise Not_found
  | Node {left=Empty; value} -> value
  | Node {left} -> min left

  let rec min_opt = function
  | Empty -> None
  | Node{left=Empty; value} -> Some value
  | Node{left} -> min_opt left

  let rec max = function
  | Empty -> raise Not_found
  | Node{value; right=Empty} -> value
  | Node{right} -> max right

  let rec max_opt = function
  | Empty -> None
  | Node{value; right=Empty} -> Some value
  | Node{right} -> max_opt right

  (* Remove the smallest elt of the given set *)
  let rec remove_min = function
  | Empty -> invalid_arg "Set.remove_min_elt"
  | Node {left=Empty; right} -> right
  | Node {left; value; right} -> create_ob3 (remove_min left) value right

  (* Merge two trees l and r into one.
     All elts of l must precede the elts of r.
     Assume | height l - height r | <= 2. *)
  let merge t1 t2 = match (t1, t2) with
  | (Empty, t) -> t
  | (t, Empty) -> t
  | (_, _) -> create_ob3 t1 (min t2) (remove_min t2)

  (* Merge two trees l and r into one.
     All elts of l must precede the elts of r.
     No assumption on the heights of l and r. *)
  let concat t1 t2 = match (t1, t2) with
  | (Empty, t) -> t
  | (t, Empty) -> t
  | (_, _) -> create_ob t1 (min t2) (remove_min t2)

  (* Splitting.  split x s returns a triple (l, present, r) where
      - l is the set of elts of s that are < x
      - r is the set of elts of s that are > x
      - present is false if s contains no elt equal to x,
        or true if s contains an elt equal to x. *)
  let rec split (x:elt) = function
  | Empty -> (Empty, false, Empty)
  | Node {left; value; right} ->
    let c = X.compare x value in
    if c = 0 then (left, true, right)
    else if c < 0 
    then
      let (ll, pres, rl) = split x left 
      in (ll, pres, create_ob rl value right)
    else
      let (lr, pres, rr) = split x right 
      in (create_ob left value lr, pres, rr)

  (* Implementation of the set operations *)
  let empty = Empty

  let is_empty = function Empty -> true | _ -> false

  let rec remove (x:elt) = function
  | Empty -> Empty
  | (Node {left; value; right} as t) ->
    let c = X.compare x value in
    if c = 0 then merge left right
    else if c < 0 then
      let ll = remove x left
      in if left == ll 
         then t
         else create_ob3 ll value right
    else
      let rr = remove x right 
      in if right == rr 
         then t
         else create_ob3 left value rr

  let rec union s1 s2 = match (s1, s2) with
  | (Empty, t2) -> t2
  | (t1, Empty) -> t1
  | (Node{left=l1; value=v1; right=r1; height=h1}, 
     Node{left=l2; value=v2; right=r2; height=h2}) ->
      if h1 >= h2 
      then 
        if h2 = 1 
        then add v2 s1 
        else begin
          let (l2, _, r2) = split v1 s2 
          in create_ob (union l1 l2) v1 (union r1 r2)
        end
      else
        if h1 = 1 
        then add v1 s2 
        else begin
          let (l1, _, r1) = split v2 s1 
          in create_ob (union l1 l2) v2 (union r1 r2)
        end

  let rec inter s1 s2 = match (s1, s2) with
  | (Empty, _) -> Empty
  | (_, Empty) -> Empty
  | (Node {left=l1; value=v1; right=r1}, t2) ->
     match split v1 t2 with
     | (l2, false, r2) -> concat (inter l1 l2) (inter r1 r2)
     | (l2, true, r2) -> create_ob (inter l1 l2) v1 (inter r1 r2)

  (* Same as split, but compute the left and right subtrees
     only if the pivot elt is not in the set.  The right subtree
     is computed on demand. *)

  type split_bis =
  | Found
  | NotFound of t * (unit -> t)

  let rec split_bis (x:elt) = function
  | Empty -> NotFound (Empty, (fun () -> Empty))
  | Node {left; value; right} ->
    let c = X.compare x value in
    if c = 0 
    then Found
    else if c < 0
    then match split_bis x left with
    | Found -> Found
    | NotFound (ll, rl) -> 
        NotFound (ll, (fun () -> create_ob (rl ()) value right))
    else match split_bis x right with
    | Found -> Found
    | NotFound (lr, rr) -> NotFound (create_ob left value lr, rr)

  let rec disjoint s1 s2 = match (s1, s2) with
  | (Empty, _) | (_, Empty) -> true
  | (Node{left=l1; value=v1; right=r1}, t2) ->
    if s1 == s2 
    then false
    else match split_bis v1 t2 with
    | NotFound(l2, r2) -> disjoint l1 l2 && disjoint r1 (r2 ())
    | Found -> false

  let rec diff s1 s2 = match (s1, s2) with
  | (Empty, _) -> Empty
  | (t1, Empty) -> t1
  | (Node{left=l1; value=v1; right=r1}, t2) ->
    match split v1 t2 with
    | (l2, false, r2) -> create_ob (diff l1 l2) v1 (diff r1 r2)
    | (l2, true, r2) -> concat (diff l1 l2) (diff r1 r2)

  type enumeration = End | More of elt * t * enumeration

  let rec cons_enum s e = match s with
  | Empty -> e
  | Node{left; value; right} -> cons_enum left (More(value, right, e))

  let rec compare_aux e1 e2 = match (e1, e2) with
  |  (End, End) -> 0
  | (End, _)  -> -1
  | (_, End) -> 1
  | (More(v1, r1, e1), More(v2, r2, e2)) ->
    let c = X.compare v1 v2 in
    if c <> 0
    then c
    else compare_aux (cons_enum r1 e1) (cons_enum r2 e2)

  let compare s1 s2 =
    compare_aux (cons_enum s1 End) (cons_enum s2 End)

  let equal s1 s2 =
    compare s1 s2 = 0

  let rec subset s1 s2 = match (s1, s2) with
  | Empty, _ -> true
  | _, Empty -> false
  | Node {left=l1; value=v1; right=r1}, 
    (Node {left=l2; value=v2; right=r2} as t2) ->
      let c = X.compare v1 v2 in
      if c = 0 
      then subset l1 l2 && subset r1 r2
      else if c < 0 
      then subset (Node {left=l1; value=v1; right=Empty; height=0; size=0}) l2        && subset r1 t2
      else subset (Node {left=Empty; value=v1; right=r1; height=0; size=0}) r2        && subset l1 t2

  let rec fold f s accu = match s with
  | Empty -> accu
  | Node{left; value; right} -> fold f right (f value (fold f left accu))

  let rec for_all p = function
  | Empty -> true
  | Node{left; value; right} -> p value && for_all p left && for_all p right

  let rec exists p = function
  | Empty -> false
  | Node{left; value; right} -> p value || exists p left || exists p right

  let rec filter p = function
  | Empty -> Empty
  | (Node{left; value; right}) as t ->
    (* call [p] in the expected left-to-right order *)
    let l' = filter p left in
    let pv = p value in
    let r' = filter p right in
    if pv then
      if left==l' && right==r' then t else create_ob l' value r'
    else concat l' r'

  let rec partition p = function
  | Empty -> (Empty, Empty)
  | Node{left; value; right} ->
    (* call [p] in the expected left-to-right order *)
    let (lt, lf) = partition p left in
    let pv = p value in
    let (rt, rf) = partition p right in
    if pv
    then (create_ob lt value rt, concat lf rf)
    else (concat lt rt, create_ob lf value rf)

  let rec elts_aux accu = function
  | Empty -> accu
  | Node {left; value; right} -> 
      elts_aux (value :: elts_aux accu right) left

  let elts s = elts_aux [] s

  let choose = min

  let rec nth s i = match s with
  | Empty -> raise Not_found
  | Node { left; right; value; size } ->
    if i >= size then raise Not_found;
    let l = cardinal left in
    if i < l
    then nth left i
    else if i=l then value
    else nth right (i - l - 1)

  let choose_opt = min_opt

  let rand = function
  | Empty -> raise Not_found
  | Node {size} as s -> nth s (Random.int size)

  let rec find (x:elt) = function
  | Empty -> raise Not_found
  | Node {left; value; right} ->
    let c = X.compare x value in
    if c = 0 then value
    else find x (if c < 0 then left else right)

  let rec find_first_aux v0 f = function
  | Empty -> v0
  | Node {left; value; right} ->
    if f value 
    then find_first_aux value f left
    else find_first_aux v0 f right

  let rec find_first f = function
  | Empty -> raise Not_found
  | Node {left; value; right} ->
    if f value 
    then find_first_aux value f left
    else find_first f right

  let rec find_first_opt_aux v0 f = function
  | Empty -> Some v0
  | Node {left; value; right} ->
    if f value 
    then find_first_opt_aux value f left
    else find_first_opt_aux v0 f right

  let rec find_first_opt f = function
  | Empty -> None
  | Node {left; value; right} ->
    if f value
    then find_first_opt_aux value f left
    else find_first_opt f right

  let rec find_last_aux v0 f = function
  | Empty -> v0
  | Node {left; value; right} ->
    if f value 
    then find_last_aux value f right
    else find_last_aux v0 f left

  let rec find_last f = function
  | Empty -> raise Not_found
  | Node {left; value; right} ->
    if f value 
    then find_last_aux value f right
    else find_last f left

  let rec find_last_opt_aux v0 f = function
  | Empty -> Some v0 
  | Node{left; value; right} ->
    if f value 
    then find_last_opt_aux value f right
    else find_last_opt_aux v0 f left

  let rec find_last_opt f = function
  | Empty -> None
  | Node{left; value; right} ->
    if f value
    then find_last_opt_aux value f right
    else find_last_opt f left

  let rec find_opt (x:elt) = function
  | Empty -> None
  | Node{left; value; right} ->
    let c = X.compare x value in
    if c = 0
    then Some value
    else find_opt x (if c < 0 then left else right)

  let try_join l v r =
  (* [create_ob l v r] can only be called when (elts of l < v <
     elts of r); use [try_join l v r] when this property may
     not hold, but you hope it does hold in the common case *)
    if (l = Empty || X.compare (max l) v < 0)
      && (r = Empty || X.compare v (min r) < 0)
    then create_ob l v r
    else union l (add v r)

  let rec map f = function
  | Empty -> Empty
  | Node{left; value; right} as t ->
    (* enforce left-to-right evaluation order *)
    let l' = map f left in
    let v' = f value in
    let r' = map f right in
    if left == l' && value == v' && right == r' 
    then t
    else try_join l' v' r'

  let try_concat t1 t2 = match (t1, t2) with
  | (Empty, t) -> t
  | (t, Empty) -> t
  | (_, _) -> try_join t1 (min t2) (remove_min t2)

  let rec filter_map f = function
  | Empty -> Empty
  | Node {left; value; right} as t ->
    (* enforce left-to-right evaluation order *)
    let l' = filter_map f left in
    let v' = f value in
    let r' = filter_map f right in
    begin match v' with
    | Some v' ->
      if left == l' && value == v' && right == r' 
      then t
      else try_join l' v' r'
    | None -> try_concat l' r'
    end

  let of_sorted_list l =
    let rec sub n l = match n, l with
    | 0, l -> Empty, l
    | 1, x0 :: l -> singleton x0, l
    | 2, x0 :: x1 :: l ->
        Node{left=singleton x0; value=x1; right=Empty; height=2; size=2}, l
    | 3, x0 :: x1 :: x2 :: l ->
        Node{left=singleton x0; value=x1; right=singleton x2; 
             height=2; size=3}, l
    | n, l ->
      let nl = n / 2 in
      let left, l = sub nl l in
      match l with
      | [] -> assert false
      | mid :: l ->
        let right, l = sub (n - nl - 1) l in
        create_ob2 left mid right, l
    in
    fst (sub (List.length l) l)

  let of_list = function 
  | [] -> empty
  | [x0] -> singleton x0
  | [x0; x1] -> add x1 (singleton x0)
  | [x0; x1; x2] -> add x2 (add x1 (singleton x0))
  | [x0; x1; x2; x3] -> add x3 (add x2 (add x1 (singleton x0)))
  | [x0; x1; x2; x3; x4] -> add x4 (add x3 (add x2 (add x1 (singleton x0))))
  | l -> of_sorted_list (List.sort_uniq X.compare l)

  let add_seq i m =
    Seq.fold_left (fun s x -> add x s) m i

  let of_seq i = add_seq i empty

  let rec seq_of_enum_ c () = match c with
  | End -> Seq.Nil
  | More (x, t, rest) -> Seq.Cons (x, seq_of_enum_ (cons_enum t rest))

  let to_seq c = seq_of_enum_ (cons_enum c End)

  let rec snoc_enum s e = match s with
  | Empty -> e
  | Node {left; value; right} -> snoc_enum right (More(value, left, e))

  let rec rev_seq_of_enum_ c () = match c with
  | End -> Seq.Nil
  | More (x, t, rest) -> Seq.Cons (x, rev_seq_of_enum_ (snoc_enum t rest))

  let to_rev_seq c = rev_seq_of_enum_ (snoc_enum c End)

  let to_seq_from (low:elt) s =
    let rec aux low s c = match s with
    | Empty -> c
    | Node {left; value; right} ->
      begin match X.compare value low with
      | 0 -> More (value, right, c)
      | n when n<0 -> aux low right c
      | _ -> aux low left (More (value, right, c))
      end
      in seq_of_enum_ (aux low s End)

  let rec to_string = function
  | Empty -> ""
  | Node { left; right; value; size } ->
      Printf.sprintf "%s\n%s[%02d]%s"
      (string_shift (to_string left))
      (X.to_string value) size
      (string_shift (to_string right))

  let print s = Printf.printf "%s\n" (to_string s)
end

