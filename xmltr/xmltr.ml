(** Type d'un chemin en xml sous la forme element.element.element *)
type xmlpath = string list

let xmlpath = String.split_on_char '.' 
let strpath = String.concat "." 

let header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

type modification = 
  Replace of string list * string 

(** XML management *)
(** [key_in_attribute key attributes] is true whenever [key] is associated
    to a value in one of the attributes. *)
let rec key_in_attribute k = function
| (key, value) :: r -> 
     if k=key
     then true 
     else key_in_attribute k r 
| [] -> false

(** [key_with_attribute element_name key value] 
    is the identifier element-key=value *)
let key_with_attribute element key value =
  Printf.sprintf "%s-%s=%s" element key value

(** replace the first occurence of [key] in a list of attributes *)
let rec replace_first key replacement = function 
| (entry, value) :: r -> 
  if entry = key then (entry, replacement) :: r
  else (entry, value) :: (replace_first key replacement r) 
| [] -> [] 

(** replace all occurences of [key] in a list of attributes *)
let rec replace_all key replacement = function 
| (entry, value) :: r -> 
  if entry = key 
  then (entry, replacement) :: (replace_all key replacement r)
  else (entry, value) :: (replace_all key replacement r) 
| [] -> [] 

(** [string2modif s] transforms a string (entered in the command line or from 
    a file) into a modification. *)
let string2modif s = 
  let w = Str.split (Str.regexp "[:]") s |> Array.of_list in
  if Array.length w <> 2
  then failwith "Invalid syntax";
  Replace (xmlpath w.(0), w.(1)) 

(** [add_from_file modifications file] adds the modifications in [file] to the 
    list [modifications]. *)
let from_file fichier =
  let ans : modification list ref = ref [] in 

  let f = open_in fichier in 
  try while true do
    let l= input_line f in 
    ans := string2modif l :: !ans;
  done with End_of_file -> close_in f; !ans

(** The main function. [modify modifications xml_tree] returns an XML tree identical
   to [xml_tree] except where the required modifications hae been applied. *)
let rec modify m = function 
| Xml.Element (e, att, cnt) as elem -> begin
    let keys = e :: List.map (fun (a, v) -> key_with_attribute e a v) att in 
    match m with 
    | Replace ([key], repl) -> 
      if key_in_attribute key att 
      then Xml.Element (e, replace_first key repl att, cnt) 
      else begin match cnt with 
      | [ PCData data ] -> 
         if List.mem key keys || key ="*" 
         then Xml.Element (e, att, [ PCData repl ]) 
         else elem
      | _ -> elem
      end
    | Replace ([key; key2], repl) when key_in_attribute key2 att -> 
        modify (Replace ([key2], repl)) elem 
    | Replace (key :: q, repl) -> 
      if List.mem key keys || key ="*" 
      then Xml.Element (e, att, List.map (modify (Replace (q, repl))) cnt)
      else elem
    | _ -> elem
    end
| PCData data -> begin 
    match m with 
    | Replace ([], repl) -> PCData repl 
    | _ -> PCData data
    end

(** Frontend functions *)

let usage="Usage : xmltr [-i] [-m modification] [-M fichier] entree.xml [sortie.xml]\n\n"^
   "xmltr manipule un fichier XML pour en changer le contenu selon\n"^
   "des règles de modifications fournies en ligne de commande ou dans\n"^
   "un fichier.\n\n"^
   "Une modification est formée d'un chemin et d'un remplacement.\n"^
   "Un chemin est de la forme\n\n"^
   "               clé.clé...clé\n\n"^
   "Les clés sont soit le nom d'un élément <NomDElement ... >\n"^
   "ou bien les attributs d'un élément <Element attribut=\"valeur\" ... >\n"^
   "Certains éléments peuvent avoir le même nom mais des attributs\n"^
   "différents <Element id=\"1\">...  <Element id=\"2\">\n"^
   "dans ce cas, il faut donner la clé souhaitée sous la forme\n\n"^
   "               Element-id=2\n\n"^
   "dans le chemin. Il est possible de remplacer une clé par '*'\n"^
   "pour modifier plusieurs éléments inclus au même niveau de\n"^
   "l'arborescence.\n"

type options = {
  mutable infile : string option;
  mutable outfile : string;
  mutable modifications : modification list
}
  

let run options = 
  let inchan = match options.infile with 
  | None-> Printf.eprintf "%s" usage; exit 1
  | Some "" -> stdin
  | Some s  -> open_in s
  in 
  let xml = ref (Xml.parse_in inchan) in 
  close_in inchan;

  List.iter (fun m -> xml := modify m !xml) options.modifications;

  let output = match options.outfile with 
  | "" -> stdout
  | s -> open_out s
  in
  Printf.fprintf output "%s" header;
  Printf.fprintf output "%s\n%!" (Xml.to_string_fmt !xml);
  close_out output 

let _ = 
  let options = { 
    infile = None; 
    outfile = ""; 
    modifications = [] 
  } in 

  Arg.parse [
  "-i", Arg.Unit (fun () -> options.infile <- None), 
        "            Lit depuis l'entrée standard";
  "-m", Arg.String (fun s -> options.modifications <- options.modifications @ [string2modif s]),
        "            Ajoute une modification.";
  "-M", Arg.String 
      (fun mfile -> options.modifications <- options.modifications @ (from_file mfile)),
      "<fichier>   Ajoute les modifications contenues dans le fichier" ]
  (fun s -> match options.infile with 
  | None -> options.infile <- Some s
  | Some _ -> options.outfile <- s)
  usage;


  run options 

