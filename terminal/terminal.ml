(** ANSI terminal management for OCaml *)
(*

   Copyright 2004 Troestler Christophe
   Christophe.Troestler(at)umons.ac.be

   Copyright 2022 Rossetto Vincent
   Vincent.Rossetto@m4x.org

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   version 3 as published by the Free Software Foundation, with the
   special exception on linking described in file LICENSE.

   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the file
   LICENSE for more details.

    @author Christophe Troestler (Christophe.Troestler@umons.ac.be)
    @author Vincent Hugot (vincent.hugot@gmail.com)
    @author Vincent Rossetto (vincent.rossetto@m4x.org) 
*)

(** This module offers basic control of ANSI compliant terminals and
    the windows shell. It is an improvement of the [ANSITerminal]
    module. 

    The functions below do not send ANSI codes (i.e., do nothing or
    only print the output) when the output is not connected to a TTY.
    Functions providing information (such as {!pos_cursor}) fail when
    in that situation.
    TTY detection is configurable by changing the value of {!isatty}.

    This library is not thread safe.
*)

(** {2 TTY checks} *)
(** TTY detection *)
let isatty = ref Unix.isatty

(** TTY detection for output channel *)
let out_channel_is_tty ch = Unix.descr_of_out_channel ch |> !isatty

(** {2 Autoreset feature} *)
(** By default, autoreset is on. *)
let autoreset = ref true

(** Turns the autoreset feature on and off.  It defaults to on. *)
let set_autoreset b = autoreset := b

(** {2:style Styles} *)
(** Basic colors available on ANSI terminals *)
type color =
| Black 
| Red 
| Green 
| Yellow 
| Blue 
| Magenta
| Cyan
| White 
| Default 
| Bright of color 
| RGB of int * int * int

(** Different decorations for the text. Some modify the brightness
    and the display, others modify the font or add elements to it. 
    The type [style] has more options than in [ANSITerminal]. 

    Some of these style may not work on every terminals. 
    For instance, my XTerm does not differentiate between [Blink]
    and [Rapidblink]. *)
type decor = 
| Reset 
| Bold
| Faint
| Italic 
| Underlined
| Blink
| Rapidblink
| Reverse
| Hidden
| Crossed
| Foreground of color
| Background of color

(** A [style] is a decor list *)
type style = decor list

(** Shortcuts for foreground and background colors *)
let black = Foreground Black
and red = Foreground Red
and green = Foreground Green
and yellow = Foreground Yellow
and blue = Foreground Blue
and bright_blue = Foreground (Bright Blue)
and magenta = Foreground Magenta
and cyan = Foreground Cyan
and white = Foreground White

and on_black = Background Black
and on_red = Background Red
and on_green = Background Green
and on_yellow = Background Yellow
and on_blue = Background Blue
and on_magenta = Background Magenta
and on_cyan = Background Cyan
and on_white = Background White
and on_gray = Background (Bright Black)

(** The default [style]. It features the default foreground and background
    as configured in the terminal. *)
let default : style = [ Foreground Default; Background Default ]

(** {2 ANSI communications} *)
(** ANSI codes *)
let e_code = "\027["

(** [send_and_read channel query fmt f] *)
(* Code inspired by http://www.ohse.de/uwe/software/resize.c.html and
   http://qemacs.sourcearchive.com/documentation/0.3.1.cvs.20050713-5/tty_8c-source.html *)
let send_and_read fdin query fmt f =
  let alarm = ref false in
  let set_alarm (_:int) = alarm := true in
  let old_alarm = Sys.signal Sys.sigalrm (Sys.Signal_handle set_alarm) in
  let tty = Unix.tcgetattr fdin in
  Unix.tcsetattr fdin Unix.TCSANOW { tty with
    Unix.c_ignbrk = false; 
    c_brkint = false; 
    c_parmrk = false;
    c_istrip = false; 
    c_inlcr = false; 
    c_igncr = false; 
    c_icrnl = false;
    c_ixon = false;  
    c_opost = true;
    c_csize = 8;  
    c_parenb = false;  
    c_icanon = false; 
    c_isig = false;
    c_echo = false; 
    c_echonl = false;
    c_vmin = 1; 
    c_vtime = 0 };
  let restore() =
    ignore(Unix.alarm 0);
    Unix.tcsetattr fdin Unix.TCSANOW tty;
    Sys.set_signal Sys.sigalrm old_alarm in
  let buf = Bytes.make 127 '\000' in
  (* FIXME: make it more robust so that it ignores previous key pressed. *)
  let rec get_answer pos =
    let l = Unix.read fdin buf pos 1 in
    let buf = Bytes.unsafe_to_string buf in (* local use only *)
    try Scanf.sscanf buf fmt f (* bail out as soon as enough info is present *)
    with Scanf.Scan_failure _ ->
      if !alarm || pos = 126 then failwith "Terminal.get_answer"
      else if buf.[pos] = '\000' then get_answer pos
      else get_answer (pos + l) in
  try
    ignore(Unix.write fdin query 0 (Bytes.length query));
    ignore(Unix.alarm 1);
    let r = get_answer 0 in
    restore();
    r
  with e ->
    restore();
    raise e

(** {3 Generic printing functions} *)
let print_code code_format = 
  Printf.printf  ("%s" ^^ code_format ^^ "%!") e_code
let eprint_code code_format = 
  Printf.eprintf ("%s" ^^ code_format ^^ "%!") e_code
let fprint_code outchan code_format = 
  Printf.fprintf outchan ("%s" ^^ code_format ^^ "%!") e_code
let sprint_code code_format = 
  Printf.sprintf ("%s" ^^ code_format ^^ "%!") e_code
let kprint_code fmt code_format = 
  Printf.ksprintf fmt ("%s" ^^ code_format ^^ "%!") e_code

(** {3 Cursor positioning} *)
(** [set_cursor x y] positions the cursor at coordinates (x,y). *)
let set_cursor x y =
  if out_channel_is_tty stdout then begin
    if x <= 0 then begin
      if y > 0 then 
        print_code "%id" y
    end else if y <= 0 then (* x > 0 *) 
      print_code "%iG" x
    else 
      print_code "%i;%iH" y x
  end

(** [move_cursor x y] moves the cursor along the vector (x,y). 
    Negative values are allowed. *)
let move_cursor x y =
  if out_channel_is_tty stdout then begin
    if x > 0 
    then print_code "%iC" x
    else if x < 0 
    then print_code "%iD" (-x);

    if y > 0 
    then print_code "%iB" y
    else if y < 0 
    then print_code "%iA" (-y)
  end

(** [save_cursor ()] stores the current position of the cursor in memory. *)
let save_cursor () =
  if out_channel_is_tty stdout then print_code "s"

(** [restore_cursor ()] place the cursor at the position stored in memory. *)
let restore_cursor () =
  if out_channel_is_tty stdout then print_code "u"

(** [move_bol ()] moves the cursor to the beginning of the line. *)
let move_bol () = Printf.printf "\r%!"

(** [pos_cursor ()] output the current coordinates of the cursor. *)
let pos_cursor () = 
  let query = Bytes.of_string (Printf.sprintf "%s6n" e_code) in
  if out_channel_is_tty stdout then begin
    try
      send_and_read Unix.stdin query ("\027[%d;%dR") (fun y x -> (x,y))
    with _ -> failwith "Terminal.pos_cursor"
  end else 
    failwith "Terminal.pos_cursor: not a TTY"

(** [resize w h] resizes the terminal to [w] columns and [h] rows. *)
let resize width height =
  if out_channel_is_tty stdout then begin
    if width <= 0 
    then invalid_arg "Terminal.resize: width <= 0";
    if height <= 0 then 
    invalid_arg "Terminal.resize: height <= 0";
    print_code "8;%i;%it" height width
  end

(** [loc] defines locations for some operations *)
type loc = Eol | Above | Below | Screen

(** [erase loc] erases the given location. *)
let erase loc =
  if out_channel_is_tty stdout then begin
    match loc with
    | Eol -> "K"
    | Above -> "1J"
    | Below ->"0J"
    | Screen -> "2J"
  end |> print_code

(** [scroll n] scrolls [n] lines. Negative values allowed. *)
let scroll lines =
  if out_channel_is_tty stdout then begin
    if lines > 0 
    then print_code "%iS" lines
    else if lines < 0 
    then print_code "%iT" (- lines)
  end

(** {3:color Colors} *)
(** [color_to_string color] returns the ANSI string code for the given [color]. *)
let rec color_to_string = function
| Black    -> "0"
| Red      -> "1"
| Green    -> "2"
| Yellow   -> "3"
| Blue     -> "4"
| Magenta  -> "5"
| Cyan     -> "6"
| White    -> "7"
| Bright c -> color_to_string c
| RGB (r,g,b) -> Printf.sprintf "8;2;%d;%d;%d" r g b
| Default  -> "9"

(** [decor_to_string decor] returns the ANSI string code for the given [decor]. *)
let rec decor_to_string = function 
| Reset      -> "0"
| Bold       -> "1"
| Faint      -> "2"
| Italic     -> "3"
| Underlined -> "4"
| Blink      -> "5"
| Rapidblink -> "6"
| Reverse    -> "7"
| Hidden     -> "8"
| Crossed    -> "9"
| Foreground (Bright (RGB (r,g,b))) -> decor_to_string (Foreground (RGB (r,g,b)))
| Background (Bright (RGB (r,g,b))) -> decor_to_string (Background (RGB (r,g,b)))
| Foreground (Bright c) ->  "9"^(color_to_string c)
| Background (Bright c) -> "10"^(color_to_string c)
| Foreground c -> "3"^(color_to_string c)
| Background c -> "4"^(color_to_string c)

(** Foreground and background colors *)
(** [fg r g b] is the foreground of RGB components [r], [g], [b]. *)
let fg r g b = Foreground (RGB (r,g,b))
(** [bg r g b] is the background of RGB components [r], [g], [b]. *)
let bg r g b = Background (RGB (r,g,b))

(** {2 Printing using styles} *)
(** [style_to_string style] converts the given {!style} into its ANSI code *)
let style_to_string (style : style) =
  e_code ^ (List.map decor_to_string style |> String.concat ";") ^ "m"

(** [fprintf out_channel style fmt] is a print format with color style 
    writing to the given out_channel. *)
let fprintf outchan style fmt = 
  if out_channel_is_tty outchan then begin
    if !autoreset  
    then Printf.fprintf outchan ("%s" ^^ fmt ^^ "\027[0m") (style_to_string style)
    else Printf.fprintf outchan ("%s" ^^ fmt) (style_to_string style)
  end else 
    Printf.fprintf outchan fmt 

(** [printf style fmt] prints to the standard output. *)
let printf style fmt = 
  if out_channel_is_tty stdout then begin
    if !autoreset 
    then Printf.printf ("%s" ^^ fmt ^^ "\027[0m") (style_to_string style)
    else Printf.printf ("%s" ^^ fmt) (style_to_string style)
  end else 
    Printf.printf fmt 

(** [eprintf style fmt] prints to the standard error channel. *)
let eprintf style fmt = 
  if out_channel_is_tty stderr then begin
    if !autoreset 
    then Printf.eprintf ("%s" ^^ fmt ^^ "\027[0m") (style_to_string style)
    else Printf.eprintf ("%s" ^^ fmt) (style_to_string style)
  end else 
    Printf.printf fmt 

(** [sprintf style fmt] returns a string with the given style commands. *)
let sprintf style fmt = 
  if !autoreset 
  then Printf.sprintf ("%s" ^^ fmt ^^ "\027[0m") (style_to_string style)
  else Printf.sprintf ("%s" ^^ fmt) (style_to_string style)
