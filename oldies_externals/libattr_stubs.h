#include <stdlib.h>
#include <fcntl.h>
#include <byterun/io.h>
#include <byterun/signals.h>
#include <byterun/mlvalues.h>
#include <byterun/memory.h>
#include <byterun/alloc.h>
#include <byterun/custom.h>
#include <attr/attributes.h>
#include <attr/xattr.h>
#include <attr/libattr.h>
#include <attr/error_context.h>

value caml_setxattr(value,value,value,value,value);

