(** {2 [Graphics] extensions} *)
type pic = {
  width : int;
  height: int;
  depth : int;
  image : Graphics.color array array
}

let load_ppm filename = 
  let image_file=open_in filename in
  let line=ref "" in
  if input_line image_file <> "P6" then 
    failwith "Not a PPM image";

  while !line = "" || !line.[0]='#' do 
    line := input_line image_file
  done;

(* The next line is the size of image *)
  let (width,height) = Scanf.sscanf !line "%d %d" (fun x y -> (x,y)) in 
  let image = Array.make_matrix height width white in
  let depth = int_of_string (input_line image_file) in
  for i=0 to height-1 do
    for j=0 to width-1 do 
      let r=input_byte image_file 
      and g=input_byte image_file 
      and b=input_byte image_file in
      image.(i).(j) <- rgb ((r*255)/ depth) ((g*255)/ depth) ((b*255)/ depth)
    done 
  done;
  close_in image_file;
  { width; height; depth; image}

let image_dimensions image = image.width, image.height

let draw_pic p = draw_image (make_image p.image)
