#include "libattr_stubs.h"

value caml_setxattr(value path, value name, value valeur, value size, value flags){
  CAMLparam5(path,name,valeur,size,flags);
  int ret=setxattr(String_val(path),String_val(name),String_val(valeur),Int_val(size),Int_val(flags));
  CAMLreturn(Val_int(ret));
}
