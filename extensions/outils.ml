(** Fichiers d'outils utiles venant de divers petits *)
(** programmes *)

(** \texttt{Argument.ml} :destiné à la gestion avancée des arguments *)
(** Lit un argument chaîne de caractères. *) 
(** Si l'argument est un nombre, positionne toutes les *)
(** variables sur la même valeur. *)
(** Si c'est une plage [a\_b], *)
(** les deux variables reçoivent les deux différentes valeurs *)
(** et la variable générique est positionnée au milieu. *)
let int_range s a v b=
  if String.contains s '_' then begin
    let liste=Str.split (Str.regexp "_") s in
    if List.length liste = 2 then begin
      a := int_of_string (List.hd liste);
      b := int_of_string (List.tail liste);
     end
   end else begin
     a := int_of_string s;
     b := !a;
   end;
   v := (!a + !b)/2

let float_range s a v b=
  if String.contains s '_' then begin
    let liste=Str.split (Str.regexp "_") s in
    if List.length liste = 2 then begin
      a := float_of_string (List.hd liste);
      b := float_of_string (List.tail liste);
     end
   end else begin
     a := float_of_string s;
     b := !a;
   end;
   v := (!a +. !b) /. 2.

(** \texttt{Date.ml} :destiné à la gestion des dates *)

let int_to_jour=function
  0 -> "dimanche"
| 1 -> "lundi"
| 2 -> "mardi"
| 3 -> "mercredi"
| 4 -> "jeudi"
| 5 -> "vendredi"
| 6 -> "samedi"
| _ -> failwith "int_to_jour : argument invalide"

let int_to_mois=function
  0 -> "janvier"
| 1 -> "février"
| 2 -> "mars"
| 3 -> "avril"
| 4 -> "mai"
| 5 -> "juin"
| 6 -> "juillet"
| 7 -> "août"
| 8 -> "septembre"
| 9 -> "octobre"
| 10 -> "novembre"
| 11 -> "décembre"
| _ -> failwith "int_to_mois : argument invalide"

let date_string()= 
  let temps=Unix.localtime (Unix.gettimeofday ()) in
  let j=temps.Unix.tm_mday
  and m=temps.Unix.tm_mon + 1
  and a=temps.Unix.tm_year+1900 in
  (if j<10 then "0" else "")^(string_of_int j)^"/"^
  (if m<10 then "0" else "")^(string_of_int m)^"/"^
  (string_of_int a)
  
let datet_string()= (* Date en texte *)
  try 
    let temps=Unix.localtime (Unix.gettimeofday ()) in
    let t=temps.Unix.tm_mday 
    and m=temps.Unix.tm_mon
    and a=temps.Unix.tm_year+1900 
    and j=temps.Unix.tm_wday in
    (int_to_jour j)^" "^
    (if t=1 then "1er" else string_of_int t)^" "^
    (int_to_mois m)^" "^
    (string_of_int a)
  with Not_found -> ""

let heure_string()=
  try 
    let temps=Unix.localtime (Unix.gettimeofday ()) in
    let h=temps.Unix.tm_hour
    and m=temps.Unix.tm_min 
    and s=temps.Unix.tm_sec in
    (if h<10 then "0" else "")^(string_of_int h)^":"^
    (if m<10 then "0" else "")^(string_of_int m)^":"^
    (if s<10 then "0" else "")^(string_of_int s)
  with Not_found -> ""

