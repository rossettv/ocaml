#include <stdlib.h>
#include <fcntl.h>
#include <byterun/io.h>
#include <byterun/signals.h>
#include <byterun/mlvalues.h>
#include <byterun/memory.h>
#include <byterun/alloc.h>
#include <byterun/custom.h>

value caml_posix_fallocate(value,value,value);
