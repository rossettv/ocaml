(** [Array] extensions *)

(** {2 Logical runs through the array} *)
(** [count p a] counts the number of elements satisfying the
    predicate [p] in the array [a]. *)
let count p = Array.fold_left (fun c x -> if p x then c+1 else c) 0

(** {2 Search on the elements of arrays} *)

(** [find_index p a] finds the smallest index of an element satisfying 
   the predicate [p]. 

   Raises [Not_found] if no elements satisfy the predicate. 

   (Implemented in [Stdlib] since [OCaml] 5.1.0) *)
let find_index p a = 
  let n = Array.length a
  and i =ref 0 in 
  while !i < n && not (p a.(!i)) do incr i done;
  if !i = n then raise Not_found else !i

(** [find_rindex p a] finds the largest index of an element satisfying 
   the predicate [p]. 
   Raises [Not_found] if no elements satisfy the predicate *)
let find_rindex p a = 
  let i = ref (Array.length a - 1) in 
  while !i >= 0 && not (p a.(!i)) do decr i done;
  if !i < 0 then raise Not_found else !i

(** [find_indices f a] finds all indices in [a] 
    where the element satisfies the predicate [p]. *)
let find_indices p a = 
  let r = ref [] in
  Array.iteri (fun i x -> if p x then r := i :: !r) a;
  !r

(** [find_all p a] finds all elements in [a] satisfying the predicate [p]. *)
let find_all p a = 
  let r = ref [] in
  Array.iter (fun x -> if p x then r := x :: !r) a;
  !r

(** [value_index v a] finds the smallest index in [a] 
    where the element is equal to [v] *)
let value_index v = find_index ((=) v)

(** [value_rindex v a] finds the largest index in [a] 
    where the element is equal to [v] *)
let value_rindex v = find_rindex ((=) v)

(** [value_indices v a] finds all indices in [a] 
    where the element is equal to [v] *)
let value_indices v = find_indices ((=) v)

(** {2 Zipping} *)
(** [zip a b] returns an [array] of pairs of elements from [a] and [b]. 
    If the arrays are of different lenghts, raises 
    [Invalid_argument "Array.zip"]. *)
let zip a b =
  let n =Array.length a in
  if Array.length b <> n then raise (Invalid_argument "Array.zip");
  Array.init n (fun i -> a.(i), b.(i))

(** [unzip a] returns a pair of arrays containing respectively the 
    first and second elements of [a]. *)
let unzip a =
  (Array.map fst a, Array.map snd a) 

(** {2 Permutations : the [Array.Permutation] module} *)
(** The permutation type : [int array] is such that [a.(i)] is 
    {%latex:$\sigma(i)$%}. *)
module Permutation = struct 
  type cycle = int list 
  type t =  int array

(** The identity permutation of {%latex:$S_n$%} : 
    {%latex:$\forall i,\,\sigma(i)=i$%}. *)
  let id n : t = Array.init n Fun.id 

(** [let b=map p a] is a new array with permuted elements of [a]. 
    The element [b.(i)] is equal to [a.(p.(i))]. *)
  let map (p : t) a =
    Array.map (Array.get a) p

(** [prod p1 p2] is the product of two permutations [p1] and [p2]. *)
  let prod : t -> t -> t = map

(** [apply a p] permutes the elements of [a] such that [a.(i)] 
    is moved to [a.(p.(i))] in-place. *)
  let apply a (p:t) = 
    Array.blit (map p a) 0 a 0 (Array.length a)

(** [sort compare a] sorts the array [a] and returns the permutation
    corresponding to the permutation between the original [a] and the 
    sorted [a]. *)
  let sort c a : t =
    let n=Array.length a in 
    let i=Array.init n Fun.id 
    and compare (x, _) (y, _) = c x y in
    let b = zip a i in
    Array.sort compare b;
    Array.blit (Array.map fst b) 0 a 0 n;
    (Array.map snd b) 

  (** [of_cycle n c] transforms a cycle [c] into a permutation 
      of {%latex:$S_n$%}. 
      Raises [Invalid_argument Array.Permutation.of_cycle] 
      if [n] is too small. *)
  let of_cycle n (c:cycle) : t =
    let a = Array.init n Fun.id in
    if c <> [] then begin try
      let i = List.hd c
      and l = ref c in
      while List.length !l > 1 do
        let j=List.hd !l in
        l := List.tl !l; 
        a.(j) <- List.hd !l
      done;
      a.(List.hd !l) <- i
    with Invalid_argument _ -> 
      raise (Invalid_argument "Array.Permutation.of_cycle");
    end;
    a

  (** [of_cycles n cycle_list] returns the permutation made of the
      composition of the cycles in [cycle_list] *)
  let of_cycles n (cl : cycle list) : t = 
    match List.length cl with 
    | 0 -> id n 
    | 1 -> of_cycle n (List.hd cl)
    | _ -> let a = of_cycle n (List.hd cl) in
           List.iter (apply a) (List.tl cl |> List.map (of_cycle n));
           a
  
  (** [rev_cycles p] computes the reversed cycles of [p]. *)
  let rev_cycles (p:t) : (cycle list) = 
    let r=ref []
    and n=Array.length p in 
    let b=Array.make n true in
    for i=0 to n-1 do
      if b.(i) && p.(i) <> i then begin
        let c=ref[i] 
        and j=ref p.(i) in
        b.(i) <- false;
        while b.(!j) do
          b.(!j) <- false;
          c := !j :: !c;
          j := p.(!j)
        done;
        r := !c :: !r
      end
    done;
    List.rev !r

(** [cycles p] returns the list of non-trivial cycles of [p]. *)
  let cycles p : (cycle list) =
    List.map List.rev (rev_cycles p) 

  (** [inv p] returns the inverse permutation of [p]. *)
  let inv (p:t) : t = 
    of_cycles (Array.length p) (rev_cycles p)

  (** [order p] is the order of the permutation, that is the
      lowest natural {%latex:$n$%} such that {%latex:$\sigma^n$%} is
      the identity permutation. *)
  let order (p:t) =
    let nl = rev_cycles p |> List.map List.length in 
    List.fold_left Int_ext.lcm 1 nl
end 

(** {2 Extractions} *)
(** [tl a] Returns a fresh [array] containing a copy of [a] without the last 
    element. *)
let tl a = 
  Array.sub a 1 (Array.length a-1)

(** [to_tuple a] returns a pair with the first two elements of [a]. *)
let to_2uple a = (a.(0), a.(1))

(** Creates a triplet with the first three elements of the input array. *)
let to_3uple a = (a.(0), a.(1), a.(2))

(** Creates a quadruplet with the first four elements of the input array. *)
let to_4uple a = (a.(0), a.(1), a.(2), a.(3))

(** {2 Application of functions} *)
(** [apply f a] modifies [a] in-place using the function [f]. *)
let apply f a = 
  Array.iteri (fun i x -> a.(i) <- f x) a

let applyi f a =
  Array.iteri (fun i x -> a.(i) <- f i x) a

(** [apply2 f a b] modifies [a] in-place using elements of [b] as second 
    argument of the function [f]. *)
let apply2 f a b = 
  Array.iteri (fun i x -> a.(i) <- f x b.(i)) a

(** [map3 f a b c] applies [f] to the sequence of arguments of arrays [a], [b] and [c]. *)
let map3 f a b c =
  let n = Array.length a 
  and lb = Array.length b 
  and lc = Array.length c in
  if n <> lb || n <> lc then
    invalid_arg "Array.map3: arrays must have the same length"
  else begin
    if n = 0 
    then [||] 
    else begin
      let r = Array.make n 
        (f (Array.unsafe_get a 0) (Array.unsafe_get b 0) (Array.unsafe_get c 0)) in
      for i = 1 to n - 1 do
        Array.unsafe_set r i 
          (f (Array.unsafe_get a i) (Array.unsafe_get b i) (Array.unsafe_get c i))
      done;
      r
    end
  end

(** [min2 ~m f a] finds the lowest value of function [f] 
    applied to two elements of the array [a]
    distant of at least [m] indices. 
    The default value of [m] is [1]. *)
let min2 ?(m=1) f a =
  let n=Array.length a in
  if n < m+1 then failwith "min2 : not enough points";
  let r0 = ref (f a.(0) a.(m))
  and ii=ref 0
  and jj=ref m in

  for i=0 to n-1-m do
    for j=i+m to n-1 do
      let r= f a.(i) a.(j) in
      if r < !r0 then begin
        r0 := r;
        ii := i;
        jj := j;
      end
    done;
  done;
  (!r0, !ii, !jj) 

(** {2 Matrix operations} *)
(** Matrix initialization. [init_matrix m n f] returns an array [a]
    such that [a.(i).(j)] is equal to [f i j]. *)
let init_matrix m n f =
  Array.init m (fun i -> Array.init n (f i))

(** Submatrix extraction. 
    [submatrix a i m j n] is the sub-matrix of [a] made of the
    elements in [m] lines starting from line [i] and [n] columns
    starting fro mcolumn [j]. *)
let submatrix a i m j n = 
  Array.init m (fun k -> Array.sub a.(i+k) j n)

(** An alias for [submatrix] *)
let sub_matrix = submatrix 

(** Deep copy of a matrix. *)
let copy_matrix a=
  Array.init (Array.length a) (fun i -> Array.copy a.(i))

(** {2 Operations between [array]s and [string]s} *)
(** Creates a [char array] of from the input [string]. *)
let of_string s = Array.init (String.length s) (String.get s)

(** Makes a [string] from a [char array]. *)
let to_string = Array.fold_left (fun s c -> s^(String_ext.of_char c)) ""
