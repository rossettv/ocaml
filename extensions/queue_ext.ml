(** [Queue] extensions *)

type 'a t = 'a Queue.t

(** {2 Logical runs through the queue} *)
(** [count p q] counts the number of elements satisfying the
    predicate [p] in the queue [q]. *)
let count p (q : 'a t) =
  let n=ref 0 in  
  Queue.iter (fun x -> if p x then incr n) q;
  !n

(** [find_all p q] returns the list of all elements in [q] satisfying [p]. *)
let find_all p (q : 'a t) =
  let r = ref [] in
  Queue.iter (fun x -> if p x then r := x :: !r) q;
  !r

(** [of_list l] creates a fresh queue with the elements of the list [l]
    in the same order. *)
let of_list l : 'a t = 
  let q = Queue.create () in
  List.iter (fun x -> Queue.add x q) l;
  q
