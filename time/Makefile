SHELL=/bin/bash
BINDIR=$(HOME)/bin
MANDIR=$(HOME)/local/man
SRCDIR=$(HOME)/src
LIBDIR=$(SRCDIR)/lib
IMPORT=$(LIBDIR)
AUTOLIB=unix str
EXTLIB=
TEXHIDES=-hide Unix -hide Graphics
OPT=ocamlopt.opt -labels -I $(LIBDIR) $(AUTOLIB:%=-I +%)
CC=ocamlc.opt -labels -I $(LIBDIR) $(AUTOLIB:%=-I +%)

GRAPHICS=$(HOME)/.opam/5.0.0/lib/graphics
OCAMLDEP=ocamldep
OCAMLHTML=ocamldoc -html -charset utf8 -colorize-code -I $(LIBDIR)
OCAMLMAN=ocamldoc -man -man-mini
OCAMLDOC=ocamldoc -sort -charset utf8 -colorize-code -I $(LIBDIR) $(HIDES)

OCAMLOPT=$(OPT) $(AUTOLIB:%=%.cmxa) $(EXTLIB:%=%.cmxa)
OCAMLC=$(CC) $(AUTOLIB:%=%.cma) $(EXTLIB:%=%.cma) 

MODULES=time
PACKAGE := time
AR=$(MODULES:%=%.a)
CMXA=$(MODULES:%=%.cmxa)
CMA=$(MODULES:%=%.cma)
CMI=$(MODULES:%=%.cmi)
CMO=$(MODULES:%=%.cmo)
CMX=$(MODULES:%=%.cmx)
MLI=$(MODULES:%=%.mli)
OBJ=$(MODULES:%=%.o)
SRC=$(MODULES:%=%.ml)

ARCHIVE:=$(PACKAGE:%=%.a)
PACKA:=$(PACKAGE:%=%.cma)
PACKX:=$(PACKAGE:%=%.cmxa)

TEX=$(PACKAGE:%=%.tex)
PDF=$(PACKAGE:%=%.pdf)

.SUFFIXES: .ml .mli .cmi .cmo .cmx .cma .cmxa .o .a .tex .pdf .log .aux 

all:
	make $(PACKA) $(ARCHIVE) $(PACKX) 

install:
	mv -f $(PACKA) $(ARCHIVE) $(PACKX) $(OBJ) $(CMI) $(CMO) $(CMX) $(LIBDIR)

$(ARCHIVE): $(PACKA)
	$(OCAMLC) -a -o $@ $(OBJ)

$(PACKA): $(CMO)
	$(OCAMLC) -a -o $@ $(CMO)

$(PACKX): $(CMX)
	$(OPT) -a -o $@ $(CMX)

.o.a: 
	$(OCAMLC) -a -o $@ $<

.cmx.cmxa: 
	$(OPT) -a -o $@ $< 

.cmo.cma: 
	$(OCAMLC) -a -o $@ $< 

.ml.mli: 
	$(OCAMLC) -i $< > $@

.mli.cmi: 
	$(OCAMLC) -c -o $@ $<

.ml.cmo: 
	$(OCAMLC) -c -o $@ $<

.ml.cmx: 
	$(OCAMLOPT) -c -o $@ $<

.ml.o: 
	$(OCAMLOPT) -o $@ $<

.ml.tex: 
	$(OCAMLDOC) -latex -o $@ $<

.ml.man:
	$(OCAMLMAN) -o $@ $<

.tex.pdf:
	@sed 's/usepackage\[latin1\]{inputenc}/usepackage\[utf8\]{inputenc}/' $< \
	  > $@.tmp
	@mv -f $@.tmp $@
	pdflatex $<

doc.pdf: $(SRC)
	$(OCAMLDOC) -latex -o all_doc_tex.tex $(SRC)
	make all_doc_tex.pdf
	mv all.pdf $@
	@rm -f all_doc_tex.*
	
clean:
	rm -f *.mli *.cmi *.cmo *.cmx *.cma *.cmxa *.a *.o *.tex *.log *.aux *.bin

.depend: $(SRC)
	ocamldep $(SRC) > $@
