(** [String] extensions *)
(** [of_char c] creates a [string] of length 1 containing the character [c]. *)
let of_char = String.make 1 

(** [to_chars s] returns the list of characters of [s]. *)
let to_chars s = 
  let l = ref [] 
  and n = String.length s 
  in
 
  for i=n-1 downto 0 do
    l := s.[i] :: !l 
  done;
  !l

(** {2 Compatibility checks} *)
(** [is_number s] is true is [s] is compatible with a [float] format string. *)
let is_number s = try ignore (float_of_string s); true with _ -> false

(** [is_integer s] is true is [s] is compatible with a [int] format string. *)
let is_integer s = try ignore (int_of_string s); true with _ -> false

(** returns true of false upon success of [float_of_string]. *)
let is_float a = begin 
  try 
    let _ = float_of_string a 
    in true 
  with _ -> false 
  end 

(** [end_float s] returns [Some x] if [s] ends with a floating point expression of [x]
    and returns [None] otherwise. *)
let end_float s = 
  match String.rindex_opt s '.' with 
  | None -> None 
  | Some p ->
     let k=ref (p-1) and l = String.length s in 
     while !k >= 0 && s.[!k] >= '0' && s.[!k] <= '9' do decr k done;
     incr k;
     try Some (float_of_string (String.sub s !k (l - !k)))
     with _ -> None

(** {2 String extractions} *)
(** [tl s i] returns a string made of last characters of [s] starting
    from character [i]. *)
let tl s i =
  String.sub s i (String.length s - i)

(** [tail s n] returns a string made of the [n] last characters of [s]. *)
let tail s n=
  String.sub s (String.length s - n) n

(** {2 Concatenation} *)
(** [tl_concat sep a] removes the first element of the string array [a] 
    and concatenates the others using the separator [sep]. *)
let tl_concat sep a = 
  Array.sub a 1 (Array.length a-1) |> Array.to_list |> String.concat sep

(** [sepcat sep s_list] returns the same string as the original [String.concat]
    except that the empty strings in [s_list] are not taken into account,
    such that the separator [sep] does not appear multiple times. *) 
let rec sepcat sep = function
  | "" :: s :: l -> sepcat sep (s::l)
  | s :: "" :: l -> sepcat sep (s::l) 
  | s :: s' :: l -> s^sep^(sepcat sep (s'::l))
  | s :: [] -> s 
  | [] -> ""

(** {2 String distance} *)
(** [lev a b] is the Levenshtein distance between strings [a] and [b]. *)
let lev a b =
  let min=function 
  | x :: l -> List.fold_left Stdlib.min x l 
  | _ -> invalid_arg "String.lev" in
  let m=String.length a
  and n=String.length b in
  let d=Array.make_matrix (m+1) (n+1) 0 in
  for i=0 to m do d.(i).(0) <- i done;
  for j=0 to n do d.(0).(j) <- j done;

  for i=0 to m-1 do
    for j=0 to n-1 do
      let c=if a.[i] = b.[j] then 0 else 1 in
      d.(i+1).(j+1) <- min [d.(i).(j+1)+1; d.(i+1).(j)+1; d.(i).(j)+c]
    done
  done;
  d.(m).(n)

