(** [Int] extensions *)

(** {2 Elementary arithmetics} *)
(** [a mod b] is equal to [a] modulo [b] and takes values between [0] and [b-1]. *)
let (mod) a b =
  if a >=0 
  then a mod b
  else b - ((mod) (-a) b)

(** [pow n p] computes [n] to the power [p]. [p] must be positive
    otherwise [pow n p] returns 1. *)
let rec pow n k =
  if k <= 0 then 1
  else if k = 1 then n
  else begin
    let r = pow n (k/2) in
    if k mod 2 = 0
    then r * r
    else r * r * n
  end

(** [digits ~base n] is the number of digits of [n] in base [base]. 
    Minus signs are ignored. The default base is 10. *)
let rec digits ?(base=10) n = 
  if n<0 
  then digits ~base (-n)
  else if n < base 
  then 1
  else 1 + digits ~base (n/base)

(** [gcd m n] is the greatest common divisor of [m] and [n] computed with 
    Euler's algorithm. *)
let rec gcd u v =
  if v <> 0 then (gcd v (u mod v))
  else (abs u)
 
(** [lcm m n] is the least common multiple of [m] and [n]. *)
let lcm m n =
  match m, n with
  | 0, _ | _, 0 -> 0
  | m, n -> abs (m * n) / (gcd m n)

(** {2 Square root} *)
(** Computes the largest integer {%latex:$r$%} such that {%latex:$r^2\leqslant n$%}.
    This implementation uses a digit-by-digit algorithm and integer arithmetics. *)
let sqrt n =
  if n < 0 then failwith "Int.sqrt";
  let b = ref (1 lsl 60) 
  and m = ref n 
  and x = ref 0 in

  while !b > n do 
    b := !b lsr 2 
  done;

  while !b > 0 do 
    if !m >= !b + !x then begin 
      m := !m - !x - !b;       
      x := (!x lsr 1) + !b;
    end else 
      x := !x lsr 1;
    b := !b lsr 2
  done;
  !x

(** [is_square n] is true if and only if [n] is a square. *)
let is_square n=
  let r=sqrt n in 
  r*r = n

(** {2 Prime numbers and factorization} *)
(** [factor_list n] is the list of prime factors. 
    Each factor enters the list a number of times equal to its multiplicity in [n].
    The list starts with the largest prime factor and is non-increasing. *)
let factor_list n=
  let ans : int list ref = ref [] 
  and m = ref n 
  and k = ref 2
  in 

  while !m > 1 do
    while !m mod !k = 0 && !m > 1 do
      ans := !k :: !ans;
      m := !m / !k
    done;
    incr k
  done;
  !ans

(** [is_prime n] is true if and only if [n] is a natural prime number. *)
let is_prime n = List.length (factor_list n) = 1
   
(** [factorize n] is the list of factors of [n] as a list of pairs [(factor, power)]
   in increasing order of the prime factors. It is based on [factor_list]. *)
let factorize n=
  let f_list = factor_list n 
  and ans = ref [] 
  in

  let (f, p) = List.fold_left (fun (last, count) factor -> 
    if factor=last 
    then (last, count+1) 
    else begin 
      if count > 0  (* necessary to remove the useless 1⁰ factor *)
      then ans := (last, count) :: !ans; 
      (factor, 1) 
    end) 
    (1,0) f_list in
  (f, p) :: !ans 

(** [euler n] is Euler's totient function {%latex:$\phi(n)$%}. *)
let euler n =
  let f = factorize n in
  List.fold_left (fun ans (k, p) -> ans * pow k (p-1) * (k-1)) 1 f

(** [divisors n] is the list of the proper divisors of [n], in no particular order.
    It does include neither [1] nor [n]. *)
let divisors n =
  let p=factor_list n 
  and ans = ref [] in
  let rec products accu = function 
  | k :: l -> products (List.map (( * ) k) accu @ accu) l
  | _ -> accu in
  List.iter (fun k -> if not (List.mem k !ans) && k <> n && k>1
                      then ans := k :: !ans) 
            (products [1] p);
  !ans

(** [aliquot n] is the sum of all proper divisors of [n] plus 1. *)
let aliquot n =
  List.fold_left (+) 1 (divisors n)
