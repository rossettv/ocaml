(** {2 [Str] shortcuts} *)
(** [split_string sep s] splits the string with separator [sep], 
    that will be interpreted as a [Str.regexp]. *)
let split_string sep = Str.split (Str.regexp sep) 

(** Alias for [split_string] {%latex:\lstinline$"[ \t]+"$%}, splits at white spaces *)
let split_white =split_string "[ \t]+"

(** Remove white spaces from a string *)
let remove_white = Str.global_replace (Str.regexp "[ \t]+") ""

(** Remove white spaces at both ends of the string *)
let trim s = Str.replace_first (Str.regexp "^[ \t]+") "" s 
          |> Str.replace_first (Str.regexp "[ \t]+$") ""
