(** [Char] extensions *)
(** [to_string c] creates a [string] of length 1 containing the character [c]. *)
let to_string = String.make 1 

(** [list_to_string l] returns the string made of the characters of [l].  *)
let list_to_string l = 
  let b = Bytes.make (List.length l) '\000' in 
  List.iteri (fun i c -> Bytes.set b i c) l;
  Bytes.to_string b

(** [to_bool c] converts the char [c] into a boolean. 
    Characters '0' and '\000' convert to [false], others to [true]. *)
let to_bool = function 
| '\000' | '0' -> false
| _ -> true

(** [of_bool b] is equal to [0] if [b] is false, and to [1] if [b] is true. *)
let of_bool = function 
| true -> '1'
| false -> '0'

(** {2 Compatibility checks} *)
(** [is_odigit c] is true whenever [c] is an octal digit. *)
let is_odigit c = 
  let ascii = Char.code c in 
  ascii >= 48 && ascii < 56

(** [is_digit c] is true whenever [c] is a decimal digit. *)
let is_digit c = 
  let ascii = Char.code c in 
  ascii >= 48 && ascii < 58

(** [is_xdigit c] is true whenever [c] is a hexadecimal digit. *)
let is_xdigit c = 
  let ascii = Char.code c in 
  (ascii >= 48 && ascii < 58) || (ascii >= 65 && ascii < 71) || (ascii >= 97 && ascii < 103)
