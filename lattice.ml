type ddlattice = {
  dimension : int;
  length : int
}

let rec pow n k =
  if k <= 0 then 1
  else if k = 1 then n
  else begin
    let r = pow n (k/2) in
    if k mod 2 = 0
    then r * r
    else r * r * n
  end

let neighbours sys i =
  let r = ref [] in
  for d = 0 to sys.dimension -1 do
    let k=pow sys.length d in
    let m=k * sys.length in
    r := ((i + k) mod m + i - (i mod m)) :: !r;
    r := ((i + m - k) mod m + i - (i mod m)) :: !r
  done;
  !r

let dist2 sys ii jj =
  let r = ref 0 
  and l = sys.length
  and i = ref ii
  and j = ref jj in 


  for d = sys.dimension -1 downto 0 do
    let x' = abs (!i - !j) mod l in 
    let x  = min x' (l-x') in 
    r := !r + x * x ;
    i := !i / l;
    j := !j / l
  done;
  !r

let cubic = { dimension = 3; length = 4 };;
let n = pow cubic.length cubic.dimension;;
let site = 0
and origine = 0 ;;
List.iteri (Printf.printf "Voisin[%d]=%d\n") (neighbours cubic site);;
List.iter (fun j -> Printf.printf "d[%d, %d]²=%d\n" origine j (dist2 cubic origine j)) (neighbours cubic site);;
List.iter (fun j -> Printf.printf "d[%d, %d]²=%d\n" origine j (dist2 cubic origine j)) (List.init n (Fun.id));;

