(**)
(** {2 New operators} *)
(** [Array.append] in the spirit of {%latex:\lstinline+@+%} for lists : 
    {%latex:\lstinline+a @| b+%} is the same as [Array.append a b]. *)
let (@|) = Array.append

(** Composition operator. The function [(f @> g)] is closed and 
    for all valid input [x] the expression [(f @> g) x] is
    equivalent to [f (g x)] and to {%latex:\lstinline+g x |> f+%}. 
    Note that [f (g x)] can also be written as [f @@ g x] 
    but that the expression [f @@ g] is not valid without an
    input. *)
let (@>) f g x = f (g x)

(** Argument-passing composition operator. The argument of {%latex:\lstinline+f >| g+%} is 
    passed to [f] and the result is passed to [g]. The result of
    {%latex:\lstinline+(f >| g) x+%} is then, for any valid input [x], the same as 
    {%latex:\lstinline+f x |> g+%}
    and as [g (f x)]. *)
let (>|) f g x = g (f x)

(** Affectation function operator. The function [f >: v] 
  applies [f] to its input and stores it in the content of [ref] variable [v]. 
  For example, with [n] of type [int ref],
  the function [int_of_string >: n] has type [string -> unit]
  and the instruction [(int_of_string >: n) "12"] results
  in the content of [n] changed into [12]. 
  Note that  [f >: n] is the same as {%latex:\lstinline+fun x -> n := f x+%} and
  {%latex:\lstinline+f >| ((:=) n)+%}. *)
let (>:) f v x = v := (f x) 

