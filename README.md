# ocaml

Basic functions for OCaml code

The module **Extensions** provides extensions of the standard library, such as `Array.tail : 'a array -> 'a array`
 which return an `array` with the first element removed. 
 
 The modules provided by the command `open Extensions` contain the standard functions and the extensions such that no type interferences happen.

OCaml infix operators can contain the following characters 
`@`, `$`, `%`, `&`, `*`, `+`, `-`, `.`, `/`, `:`, `<`, `=`, `>`, `^`, `|`, 
`?`, `!`, `~` (or `#` but only in first position) 
and must not start with `!`, `?` or `~`.

keywords :
  `!=`   `#`    `&`     `&&`   `'`     `(`     `)`    `*`     `+`   `,`   `-`
  `-.`   `->`   `.`     `..`   `.~`    `:`     `::`   `:=`    `:>`  `;`   `;;`
  `<`    `<-`   `=`     `>`    `>]`    `>}`    `?`    `[`     `[<`  `[>`  `[|`
  `]`    `_`    ` `` `    `{`     `{<`    `|`     `|]`   `||`    `}`   `~`

camlp4 keywords :
  `parser`    `value`    `$`     `$$`    `$:`    `<:`    `<<`  `>>`    `??`

ocaml keywords :
   `and`  `as`  `assert`  `asr`  `begin`  `class` `constraint`  `do`  `done`  `downto`
   `else`  `end`  `exception`  `external`  `false`  `for`  `fun`  `function`  `functor`
   `if`  `in`  `include`  `inherit`  `initializer`  `land`  `lazy`  `let`  `lor`  `lsl`
   `lsr`  `lxor`  `match`  `method`  `mod`  `module`  `mutable`  `new`  `nonrec`  `object`
   `of`  `open`  `or`  `private`  `rec`  `sig`  `struct`  `then`  `to`  `true`  `try`
   `type`  `val`  `virtual`  `when`  `while`  `with` 

ocaml operators
   `land`  `lor`  `lxor`  `lnot`  `lsl`  `lsr`  `asr`  `mod`  `or` -->

`&&`  `||` : bool 
`+`  `-`  `~-`  `*`   `/`  : int
`+.`  `-.`  `~-.`  `*.`  `/.`  `**`  : float
`@`  : list 
`^`  : string 
`!`  `:=`  'a ref  
`=`  `<>`  `==` `!=`  `<` `<=`  `>`  `>=`  : comparison
`|>`  `@@` : association


