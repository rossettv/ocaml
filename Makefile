SHELL=/bin/bash
BINDIR=$(HOME)/bin
MANDIR=$(HOME)/local/man
SRCDIR=$(HOME)/src
LIBDIR=$(SRCDIR)/lib
IMPORT=$(LIBDIR)
EXTLIB=
AUTOLIB=unix str
TEXHIDES=-hide Unix -hide Graphics
OPT=ocamlopt.opt -labels -I $(LIBDIR)
CC=ocamlc.opt -labels -I $(LIBDIR)

GRAPHICS=$(HOME)/.opam/4.10.0/lib/graphics
CAMLX=$(OPT) $(AUTOLIB:%=-I +%) $(EXTLIB:%=%.cmxa)
CAMLC=$(CC) $(AUTOLIB:%=-I +%) $(EXTLIB:%=%.cma) 
CAMLD=ocamldep
CAMLH=ocamldoc -html -charset utf8 -colorize-code -I $(LIBDIR)
CAMLM=ocamldoc -man -man-mini
CAMLO=$(OPT) $(AUTOLIB:%=-I +%) $(EXTLIB:%=%.cmxa)

MODULES=extensions ordset ordbag terminal time xmltr
AR=$(MODULES:%=%.a)
ARX=$(MODULES:%=%.cmxa)
CMA=$(MODULES:%=%.cma)
CMI=$(MODULES:%=%.cmi)
CMO=$(MODULES:%=%.cmo)
CMX=$(MODULES:%=%.cmx)
MLI=$(MODULES:%=%.mli)
OBJ=$(MODULES:%=%.o)
SRC=$(MODULES:%=%.ml)

TEX=$(MODULES:%=%.tex)
PDF=$(MODULES:%=%.pdf)

OCAMLDOC:=ocamldoc -sort -charset utf8 -colorize-code -I $(LIBDIR) $(HIDES)


.SUFFIXES: .ml .mli .cmi .cmo .cmx .cma .cmxa .o .a .tex .pdf .log .aux 

all:
	for m in $(MODULES); do make -C $${m}; done

install:
	for m in $(MODULES); do make -C $${m} install; done

.o.a: 
	$(CAMLC) -a -o $@ $<

.cmx.cmxa: 
	$(CAMLX) -a -o $@ $< 

.cmo.cma: 
	$(CAMLC) -a -o $@ $< 

.ml.mli: 
	$(CAMLC) -i $< > $@

.mli.cmi: 
	$(CAMLC) -c -o $@ $<

.ml.cmo: 
	$(CAMLC) -c -o $@ $<

.ml.cmx: 
	$(CAMLX) -c -o $@ $<

.ml.o: 
	$(CAMLX) -o $@ $<

.ml.tex: 
	$(OCAMLDOC) -latex -o $@ $<

.tex.pdf:
	@sed 's/usepackage\[latin1\]{inputenc}/usepackage\[utf8\]{inputenc}/' $< \
	  > $@.tmp
	@mv -f $@.tmp $@
	pdflatex $<

doc.pdf: $(SRC)
	$(OCAMLDOC) -latex -o all_doc_tex.tex $(SRC)
	make all_doc_tex.pdf
	mv all.pdf $@
	@rm -f all_doc_tex.*
	
clean:
	rm -f *.mli *.cmi *.cmo *.cmx *.cma *.cmxa *.a *.o *.tex *.log *.aux *.bin

.depend: $(SRC)
	ocamldep $(SRC) > $@
