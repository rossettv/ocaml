(** Ordered bags.  
 
  {%latex:\textit{Note : this implementation is adapted from the%} [Set]
  {%latex:code from the Objective Caml standard library written by Xavier Leroy.}
  \bigskip%}
  {%latex:\newcommand\msbl{\\{\!\!\\{}\newcommand\msbr{\\}\!\!\\}}
    \lstset{}%} 

  A bag (or multiset) is a collection that can, contrary to a set,
  contain the same element several times. 
  The number of times an element is present is called its {%latex:\emph{multiplicity}%}. 
  A bag containing two exemplars of the element {%latex:$x$%} and three exemplars
  of the element {%latex:$y$%} is denoted by {%latex:$\msbl x\; x\; y\; y\; y\msbr$%}.
  
  More formally, a bag is a pair {%latex:$B=(E,\,m)$%} where {%latex:$E$%} is a set and 
  {%latex:$m$%} is an application {%latex:$E\to \mathbf{N}$%} such that 
  {%latex:$m(x)$%} is the multiplicity of {%latex:$x$%} in {%latex:$B$%}. While the multiplicity
  of an element can equal 0 in this definition, only elements with multiplicity at least 
  equal to 1 are represented in an instance of bag. 
*)

(* {2 Signature of the ordered type} *)

module type OrderedType =
  sig
    type t
    val compare: t -> t -> int
    val to_string: t -> string 
  end
(** The ordered bags provided by this module contain element of a single ordered type.
    The order is required to create an ordered set or bag implemented as
    a binary tree. The [to_string] function is very useful during coding and
    can be also handy in some application, it is only used in the function [to_string] 
    and [print] of the [Ordbag] module. *)

(** {2 The [Ordbag.T] module type} *)
(** The signature of a [Ordbag] module contains all the functions of a [Set] and also
    specific functions. For instance, since the elements can have a large multiplicity,
    operations performed only once for all of these elements are more efficient than 
    in arrays or lists. *)
    
module type T = sig

  (** [elt] is the type of elements of the bag.
      It is equal to [A.t] where [A] is the module of type [OrderedType]. *)
  type elt

  (** [node] is an alias for a tuple containing an element and its multiplicity. *)
  type node = elt * int

  (** Type of the ordered bag. They are implemented as balanced binary trees 
      where each node carries two more integers than
      in the current [Set] implementation in Objective Caml: the multiplicity
      of the elements and the total number of elements contained in the node and its children.  
      The type [t] is defined as being either [Empty] or [Node]. 
      Nodes contain left and right children of type [t].
      The type is therefore recursive. Most algorithms in this implementation are
      recursive and if so, they are tail-recursive. *) 
  type t

  (** {3 The empty bag} *)
  (** The empty bag {%latex:$\msbl\msbr$%}. *)
  val empty: t

  (** Emptiness test. *)
  val is_empty: t -> bool

  (** {3 Global data} *)
  (** [cardinal bag] is the total number of elements in the bag. *)
  val cardinal: t -> int

  (** [depth bag] is the number of levels of the binary tree. It is kept 
  minimal such that [depth] is approximately the logarithm of the number of nodes. *) 
  val depth : t -> int

  (** [nodes bag] is the number of nodes, that is the number of different elements
  in the bag. As several examplars of the same element are allowed, this number
  is always smaller than [cardinal s]. *)
  val nodes : t -> int

  (** {3 Single element operations} *)
  (** [mem x bag] tests if the element [x] is present in the [bag]. *)
  val mem: elt -> t -> bool

  (** [multiplicity x bag] return the number of copies of the element [x] 
      present in the [bag]. *)
  val multiplicity: elt -> t -> int

  (** [singleton x] is the bag containing a single element {%latex:$\msbl x\msbr$%}. 
      Its cardinal is equal to 1. *)
  val singleton: elt -> t

  (** [singlenode (x, m)] is the bag containing {%latex:$m$%} exemplars of the element [x],
      {%latex:$\msbl\underbrace{x,\, x,\, \dots\, x}\sb{m\rm\ times}\msbr$%}.
      Its cardinal is equal to {%latex:$m$%}. *)
  val singlenode: node -> t

  (** {3 Adding and removing elements} *)
  (** [add x bag] returns a bag containing one more exemplar of the element [x]. If
      [x] was already present the number of nodes is unchanged, but the 
      multiplicity of element [x] in [bag] is increased by 1. 
      If [x] was not present, a new node is created. *)
  val add: elt -> t -> t

  (** [add_many (x, m) bag] returns a bag containing [n] more exemplars of the 
      element [x]. If [x] was already present the number of nodes is unchanged, 
      but the multiplicity of [x] in [bag] is increased by [n].
      If [x] was not present, a new node is created. *)
  val add_many : node -> t -> t

  (** [remove x bag] returns a bag containing one less exemplar of the element [x]. 
      If [x] was not present, the returned bag is the original bag. 
      If [x] was present more than once, then the returned bag contains the same 
      nodes as the original one, with the multiplicity of element [x] 
      decreased by one. *)
  val remove: elt -> t -> t

  (** [remove_many (x, m) bag] returns a bag containing at most [n] less exemplars
      of the element [x]. If [x] was not present, the returned bag is the
      original bag.  If [x] was present, then the returned set contains the same
      nodes as the original one, with the multiplicity of element [x] reduced by [n]
      or the node removed if [n] is larger than the original multiplicity of element
      [x]. *)
  val remove_many : node -> t -> t

  (** [remove_all x bag] returns a bag where the node containing [x] has been 
      removed from the original. If the element [x] was not present, 
      the original bag is unchanged. *)
  val remove_all : elt -> t -> t

  (** {3 Binary operations on ordered bags} *)
  (** [union b1 b2] contains all elements of [b1] and [b2]. Every element present
      in both [b1] and [b2] is present with a multiplicity equal to the sum of the
      multiplicities in the input bags. *)
  val union: t -> t -> t

  (** [inter b1 b2] contains all elements present in both [b1] and [b2] with their
  lowest multiplicity. *)
  val inter: t -> t -> t

  (** [disjoint b1 b2] is true if all elements that are present in one of [b1] and 
      [b2] are not present in the other. *)
  val disjoint: t -> t -> bool

  (** [diff b1 b2] contains the elements present in [b1] but not in [b2]. If an
      element is present with different multiplicities, its multiplicity in the
      result is their difference. *)
  val diff: t -> t -> t

  (** {3 Comparison between ordered bags} *)
  (** [compare b1 b2] is the comparison function for the lexicographic order 
      inferred from the order on the elements (provided by [A.compare] to 
      the functor [Make]). In case of equality of all elements, the set with
      the lowest multiplicity for the lowest element with 
      differing multiplicities is considered the lowest multiset for this order.
      
      Note that since the signature of [Ordbag.T] is compatible with [OrderedType],
      it is possible to create ordered bags of ordered bags. *)
  val compare: t -> t -> int

  (** [equal b1 b2] is the standard equality test. *)
  val equal: t -> t -> bool

  (** [subbag b1 b2] is true if and only if [b2] is a sub-bag of [b2], that is 
  if and only if there is a bag [b'] such that [union b2 b'] is equal to [b1]. *)
  val subbag: t -> t -> bool

  (** {3 Iterators on elements} *)
  (** [iter f s] applies the function [f] to each element of the bag. 
      If an element has a multiplicity larger than one, the function [f] is applied
      that many times to the element. The application is performed in increasing
      order on the elements. 

  {5 Example}
  If [numbers] is a bag of integers then the sum of all elements
  would be stored in [sum] by the code 
   
  {[let sum = ref 0 in iter (fun k -> sum := !sum + k) numbers]} *)
  val iter: (elt -> unit) -> t -> unit

  (** [iterm f s] applies the function [f] to each node [(x, m)] for each 
  element [x] and its multiplicity [m]. 

  {5 Example}
  If [numbers] is a set of integers then the sum of all elements
  would be stored in [sum] by the code 
   
  {[let sum = ref 0 in iterm (fun (k, m) -> sum := !sum + k*m) numbers]} *)
  val iterm: (node -> unit) -> t -> unit

  (** [map f bag] returns a bag where the elements have been modified by the
  function [f]. Beware that the type of the elements must remain the same. The
  function [f] is applied to each element once and the multiplicity is preserved. *)
  val map: (elt -> elt) -> t -> t
  
  (** [umap f bag] returns a bag where the elements have all been passed to [f].
  Use only in the case where [f] is not deterministic (in other words, when it
  contains randomness or boundary effects). *) 
  val umap: (elt -> elt) -> t -> t 

  (** [mapm f s] return a bag where the nodes (elements with multiplicities) are 
      computed from nodes of the input [Ordbag]. *)
  val mapm: (node -> node) -> t -> t

  (** [fold_left f x0 s] is the result of 

  {[f (... (f (f x0 x1) x2) ...) xn]}

  where [x1,... xn] are the ordered elements of [s]. 

  {5 Example}
  If [numbers] is a set of integers then the sum of all elements
  would be given by the code 
   
  {[fold_left (+) 0 numbers]} *)
  val fold_left: ('a -> elt -> 'a) -> 'a -> t -> 'a

  (** [fold_left f s xm] is the result of 

  {[f x1 (f x2 (... (f xn xm)) ...)]}

  where [x1,... xn] are the ordered elements of [s].

  {5 Example}
  If [numbers] is a set of integers then the sum of all elements
  would be given by the code 
   
  {[fold_right (+) 0 numbers]} *)
  val fold_right: (elt -> 'a -> 'a) -> t -> 'a -> 'a

  (** {3 Logical tests} *)
  (** [for_all p bag] tests the predicate [p] on all element values in the [bag]
  and returns [true] if all elements satisfy [p]. 
  The result is independent of the multiplicities of the elements. *)
  val for_all: (elt -> bool) -> t -> bool

  (** [exists p bag] tests the predicate [p] on element values in the [bag]
  and returns [true] if at least one element satisfies [p]. 
  The result is independent of the multiplicities of the elements. *)
  val exists: (elt -> bool) -> t -> bool

  (** {3 Filtering and partioning} *)
  (** [filter p bag] returns a bag with all the elements of [bag] satisfying [p]. The
  multiplicities of the elements are preserved in the result. *)
  val filter: (elt -> bool) -> t -> t

  (** [filterm p bag] returns a bag with nodes [(x, m)] of [bag] satisfying [p]. *)
  val filterm: (node -> bool) -> t -> t

  (** [filterm_map f bag] returns a bag with the mapped nodes [(x', m')] obtained from
  nodes [(x, m)] of [bag] not returning a [None] result. 
  Beware that the type of the elements must remain the same. *)
  val filterm_map: (node -> node option) -> t -> t

  (** [partition p bag] is a pair of bags containing the nodes satisfying [p]
  and those not satisfying [p]. *)
  val partition: (node -> bool) -> t -> t * t

  (** [split x bag] returns a triplet [(b1, m, b2)] where the elements of [b1] 
  are strictly smaller than [x] and the elements of [b2] are all strictly larger 
  than [x]. The integer [m] is the multiplicity of elements equal to [x] in [bag] :
  it is [0] if [x] is not a member of [bag]. *)
  val split: elt -> t -> t * int * t

  (** {3 Picking elements} *)
  (** [min bag] is the smallest node in [bag]. *)
  val min: t -> node

  (** [min_opt bag] is [Some (min bag)] or [None] if [bag] is empty. *)
  val min_opt: t -> node option

  (** [max bag] is the largest node in [bag]. *)
  val max: t -> node

  (** [max_opt bag] is [Some (max bag)] or [None] if [bag] is empty. *)
  val max_opt: t -> node option

  (** [most bag] returns the element with the largest multiplicity. In case of equality
      the smallest such element is returned. *)
  val most: t -> node

  (** [least bag] returns the element with the smallest multiplicity. 
      In case of equality an arbitrary such element is returned. *)
  val least: t -> node

  (** [choose bag] is an unspecified element of [bag] 
      (current implementation : the smallest one). *)
  val choose: t -> elt

  (** [choose_opt bag] is [Some (choose bag)] or [None] if [bag] is empty. *)
  val choose_opt: t -> elt option

  (** [nth i bag] is the element of rank [i] starting from [0], where the rank is
  obtained with all element, counted with multiplicity, ordered from the lowest
  to the largest. *)
  val nth: t -> int -> elt

  (** [nth_node i bag] is the node of rank [i] starting from [0], where the rank is
  that of the nodes ordered from the lowest to the largest element, without taking into
  account the multiplicities *)
  val nth_node: t -> int -> node

  (** [rand bag] is a random element of [bag] chosen by the uniform probability law
  on the elements, counted with their multiplicity. *)
  val rand: t -> elt

  (** [rand_node bag] is a random node in [bag] chosen by the uniform probability law
  on the nodes. The multiplicity of the nodes is not taken into account. *)
  val rand_node: t -> node

  (** {3 Finding elements} *)
  (** [get x bag] returns the element and its multiplicity. If [x] is not in [bag],
  raises [Not_found]. *)
  val get: elt -> t -> node

  (** [get_opt x bag] returns [Some (x, m)] where [m] is the element's multiplicity. 
      If [x] is not in [bag], return [None]. *)
  val get_opt: elt -> t -> node option

  (** [find p bag] returns a node satisfying the predicate [p].
      If no elements in [bag] satisfy [p], raises [Not_found]. *)
  val find: (node -> bool) -> t -> node 

  (** [find_opt p bag] returns [Some node] where [node] satisfies the predicate [p].
      If no elements in [bag] satisfy [p], returns [None]. *)
  val find_opt: (node -> bool) -> t -> node option

  (** [count p bag] returns the number of elements satisfying the predicate [p] in [bag]. *)
  val count: (elt -> bool) -> t -> int

  (** [count_nodes p bag] returns the number of nodes satisfying the predicate [p] in [bag]. *)
  val count_nodes: (node -> bool) -> t -> int

  (** {3 Conversions} *)
  (** [to_list bag] returns a list of all elements respecting their multiplicity *)
  val to_list: t -> elt list

  (** [of_list l] returns a bag containing all elements (with multiplicity) 
      of the list [l]. *)
  val of_list: elt list -> t

  (** [to_nodes bag] returns a list of nodes [(x, m)] where [x] is an 
      element and [m]{%latex:$\geq 1$%} its multiplicity *)
  val to_nodes: t -> node list

  (** [of_nodes l] returns an ordered bag where the elements are inserted multiple
      times. Redundancy is allowed, that is an element can be present several times
      in [l] with different multiplicities. *)
  val of_nodes: node list -> t

  val to_seq_from : elt -> t -> elt Seq.t

  (** [to_seq h] converts the multiset into an increasing sequence [Seq.t] 
      respecting the mulitiplicity of elements *)
  val to_seq : t -> elt Seq.t

  (** [to_rev_seq h] converts the multiset into a decreasing sequence [Seq.t] 
      respecting the mulitiplicity of elements *)
  val to_rev_seq : t -> elt Seq.t

  (** [of_seq sq] create a multiset from all elements of the [Seq.t] sequence [sq]. *)
  val of_seq : elt Seq.t -> t

  (** [add_seq sq s] adds all elements of the [Seq.t] sequence [sq] to the set [s]. *)
  val add_seq : elt Seq.t -> t -> t
 
  (** [to_string s] returns a string representation of [s]. *)
  val to_string : t -> string

  (** [print s] prints the contents of [to_string s]. *)
  val print : t -> unit 

  (** [to_tree_string s] returns a multi-line string mimicking the
   underlying tree structure of the set [s]. *)
  val to_tree_string : t -> string
  
  (** [print_tree s] prints [to_tree_string s] to the standard output. *)
  val print_tree : t -> unit
end

(** {2 The [Ordbag.Make] functor} *)
(** The functor [Make] is defined by the functor
{%latex:\medskip

%}
    {[module Make(A: OrderedType) : T with type elt = A.t]} 
{%latex:\medskip

\noindent%}
The type [elt] of the [Ordbag] module is the same as 
the type defined as [t] in the [OrderedType] module [A]. *)
(**/**)

module Make(X: OrderedType) : T with type elt = X.t = struct
  type elt = X.t
  type node = elt * int
  type t = Empty 
  | Node of {
    left :  t; 
    value : elt; 
    qty   : int;
    right : t; 
    depth: int; 
    card  : int
  }

  (* Implementation of the set operations *)
  let empty = Empty

  let is_empty = function Empty -> true | _ -> false
  
  (* [string_shift ~dec s] *)
  let string_shift ?(dec=8) s =
    let sep = String.make dec ' ' in
    let nlsep= "\n"^sep in 
    sep^(Str.split (Str.regexp "\n") s |> String.concat nlsep)

  (** String conversions into human readble form for small multisets *)
  let rec to_tree_string = function 
  | Empty -> ""
  | Node { left; right; value; qty; card } ->
      let dec=String.length (string_of_int card) + 6 in  
      Printf.sprintf "%s\n%s {%#d}[%d]%s"
        (string_shift ~dec (to_tree_string left))
        (X.to_string value) qty card
        (string_shift ~dec (to_tree_string right))

  (** [print s] prints the result of [to_string s] to the standard output. *)
  let print_tree s = Printf.printf "%s\n" (to_tree_string s)

  let singlenode (value, qty) = 
    Node {left=Empty; value; qty; right=Empty; depth=1; card=qty}

  let singleton x = singlenode (x, 1)

  let depth = function
  | Empty -> 0
  | Node {depth} -> depth

  let cardinal = function 
  | Empty -> 0
  | Node {card} -> card

  let rec nodes = function 
  | Empty -> 0
  | Node {left; value; right} -> 1 + nodes left + nodes right

  let rec iter (f : elt -> unit) = function
  | Empty -> ()
  | Node {left; value; qty; right} ->
     iter f left; 
     for _ = 1 to qty do f value done;
     iter f right

  let rec iterm (f : elt * int -> unit) = function
  | Empty -> ()
  | Node {left; value; qty; right} ->
     iterm f left; 
     f (value, qty);
     iterm f right

  let rec mem (x:elt) = function
  | Empty -> false
  | Node {left; value; right} ->
    let c = X.compare x value in
    c = 0 || mem x (if c < 0 then left else right)

  let rec multiplicity (x:elt) = function 
  | Empty -> 0
  | Node {left; value; qty; right} ->
    let c = X.compare x value in 
    if c=0 
    then qty
    else if c < 0 
    then multiplicity x left
    else multiplicity x right

  let rec to_list_aux accu = function
  | Empty -> accu
  | Node {left; value; qty; right} -> 
    let l=ref (to_list_aux accu right) in
    for _ = 1 to qty do l := value :: !l done;
    to_list_aux !l left

  let to_list = to_list_aux [] 

  let to_string s = 
    to_list s |> List.map X.to_string |> String.concat " " |> Printf.sprintf "{ %s }"

  let print s = Printf.printf "%s\n" (to_string s)

  (** Creates a new node with left son l, value v and right son r.
     We must have all elements of l < v < all elements of r.
     l and r must be balanced and | depth l - depth r | <= 2.
     Inline expansion of depth for better speed. *)
  let create_ob2 left (value, qty) right =
    let hl = match left  with Empty -> 0 | Node {depth} -> depth in
    let hr = match right with Empty -> 0 | Node {depth} -> depth in
    Node {left; value; qty; right; 
          depth = (if hl >= hr then hl + 1 else hr + 1);
          card = cardinal left + qty + cardinal right}

  (** Same as create_ob2, but performs one step of rebalancing if necessary.
     Assumes l and r balanced and | depth l - depth r | <= 3.
     Inline expansion of create_ob2 for better speed in the most frequent case
     where no rebalancing is required. *)
  let create_ob3 l (v, q) r =
    let hl = match l with Empty -> 0 | Node {depth} -> depth in
    let hr = match r with Empty -> 0 | Node {depth} -> depth in
    if hl > hr + 2 then begin match l with
    | Empty -> invalid_arg "Ordbag.create_ob3"
    | Node {left=ll; value=lv; qty=lq; right=lr} ->
      if depth ll >= depth lr
      then create_ob2 ll (lv, lq) (create_ob2 lr (v, q) r)
      else begin match lr with
      | Empty -> invalid_arg "Ordbag.create_ob3"
      | Node {left=lrl; value=lrv; qty=lrq; right=lrr} ->
         create_ob2 (create_ob2 ll (lv, lq) lrl) (lrv, lrq) (create_ob2 lrr (v, q) r)
      end
    end else if hr > hl + 2 then begin match r with
    | Empty -> invalid_arg "Ordbag.create_ob3"
    | Node {left=rl; value=rv; qty=rq; right=rr} ->
      if depth rr >= depth rl 
      then create_ob2 (create_ob2 l (v, q) rl) (rv, rq) rr
      else begin match rl with
      | Empty -> invalid_arg "Ordbag.create_ob3"
      | Node {left=rll; value=rlv; qty=rlq; right=rlr} ->
         create_ob2 (create_ob2 l (v, q) rll) (rlv, rlq) (create_ob2 rlr (rv, rq) rr)
      end
    end else
      Node{left=l; value=v; qty=q; right=r; 
           depth = (if hl >= hr then hl + 1 else hr + 1);
           card = cardinal l + q + cardinal r}

  (* Insertion of element with multiplicity *)
  let rec add_many (v, q) = function 
  | Empty -> singlenode (v, q) 
  | Node {left; value; qty; right; depth; card} -> 
    let c = X.compare v value in 
    if c = 0 then
      Node {left; value; qty=qty+q; right; depth; card=card+q}
    else if c < 0 then
      let ll = add_many (v, q) left in
      create_ob3 ll (value, qty) right
    else
      let rr = add_many (v, q) right in 
      create_ob3 left (value, qty) rr 

  (* Insertion of a single element *)
  let add v = add_many (v, 1)

  (* Smallest and greatest element of a set *)
  let rec min = function
  | Empty -> raise Not_found
  | Node {left=Empty; value; qty } -> (value, qty)
  | Node {left} -> min left

  let rec min_opt = function
  | Empty -> None
  | Node{left=Empty; value; qty} -> Some (value, qty)
  | Node{left} -> min_opt left

  let rec max = function
  | Empty -> raise Not_found
  | Node{value; qty; right=Empty} -> (value, qty)
  | Node{right} -> max right

  let rec max_opt = function
  | Empty -> None
  | Node{value; qty; right=Empty} -> Some (value, qty)
  | Node{right} -> max_opt right

  (* Beware: those two functions assume that the added v is *strictly*
     smaller (or bigger) than all the present elements in the tree; it
     does not test for equality with the current min (or max) element.
     Indeed, they are only used during the "create_ob" operation which
     respects this precondition.
  *)
  let rec add_min_elt (x, q) = function
  | Empty -> singlenode (x, q)
  | Node {left; value; qty; right} ->
    create_ob3 (add_min_elt (x,q) left) (value, qty) right

  let rec add_max_elt (x,q) = function
  | Empty -> singlenode (x, q)
  | Node {left; value; qty; right} ->
    create_ob3 left (value,qty) (add_max_elt (x,q) right)

  (* Same as create_ob2 and create_ob3, but no assumptions are made on the
     relative depths of l and r. *)
  let rec create_ob l (v, q) r = match (l, r) with
  | (Empty, _) -> add_min_elt (v,q) r
  | (_, Empty) -> add_max_elt (v,q) l
  | (Node{left=ll; value=lv; qty=lq; right=lr; depth=lh}, 
     Node{left=rl; value=rv; qty=rq; right=rr; depth=rh}) ->
      if lh > rh + 2 
      then create_ob3 ll (lv, lq) (create_ob lr (v, q) r)
      else if rh > lh + 2 
      then create_ob3 (create_ob l (v, q) rl) (rv, rq) rr
      else create_ob2 l (v, q) r

  (* Remove the smallest element of the given set *)
  let rec remove_min = function
  | Empty -> invalid_arg "Ordbag.remove_min_elt"
  | Node {left=Empty; right} -> right
  | Node {left; value; qty; right} -> create_ob3 (remove_min left) (value, qty) right

  (* Merge two trees l and r into one.
     All elements of l must precede the elements of r.
     Assume | depth l - depth r | <= 2. *)
  let merge t1 t2 = match (t1, t2) with
  | (Empty, t) -> t
  | (t, Empty) -> t
  | (_, _) -> create_ob3 t1 (min t2) (remove_min t2)

  (* Merge two trees l and r into one.
     All elements of l must precede the elements of r.
     No assumption on the depths of l and r. *)
  let concat t1 t2 = match (t1, t2) with
  | (Empty, t) -> t
  | (t, Empty) -> t
  | (_, _) -> create_ob t1 (min t2) (remove_min t2)

  (* Splitting. [split x bag] returns a triple [(l, m, r)] where
      - [l] is the bag of elements of [bag] that are < [x]
      - [r] is the bag of elements of [bag] that are > [x]
      - [m] is the multiplicity of [x] in [bag] (0 if none are present). *)
  let rec split (x:elt) = function
  | Empty -> Empty, 0, Empty
  | Node {left; value; qty; right} ->
    let c = X.compare x value in
    if c = 0 then left, qty, right 
    else if c < 0 then
      let (ll, nbr, rl) = split x left in
      ll, nbr, create_ob rl (value, qty) right
    else
      let (lr, nbr, rr) = split x right in
      create_ob left (value, qty) lr, nbr, rr

  let rec remove_many (x, m) = function
  | Empty -> Empty
  | Node {left; value; qty; right} ->
    let c = X.compare x value in
    if c = 0 && qty <= m then merge left right
    else if c=0 then create_ob3 left (value, qty-m) right
    else if c < 0 then
      let ll = remove_many (x, m) left
      in create_ob3 ll (value, qty) right
    else
      let rr = remove_many (x, m) right 
      in create_ob3 left (value,qty) rr

  let remove x = remove_many (x, 1)

  let rec remove_all (x:elt) = function
  | Empty -> Empty
  | (Node {left; value; qty; right} as t) ->
    let c = X.compare x value in
    if c = 0 then merge left right
    else if c < 0 then
      let ll = remove x left in
      if ll == left 
      then t 
      else create_ob3 ll (value, qty) right
    else
      let rr = remove x right in
      if rr == right 
      then t 
      else create_ob3 left (value,qty) rr

  let rec most_aux (x, q) = function 
  | Empty -> (x, q)
  | Node {left; value; qty; right; card} -> 
      if card < q 
      then (x, q)
      else begin 
        let (x1, q1) = 
          if qty > q 
          then (value, qty) 
          else (x, q) 
        and cl = cardinal left 
        and cr = cardinal right in 
        if cl < q1 && cr < q1 then (x1, q1) 
        else if cl < q1 
        then most_aux (x1, q1) right
        else begin 
          let (x2, q2) = most_aux (x1, q1) left in
          if cr < q2 
          then (x2, q2) 
          else most_aux (x2, q2) right
        end
      end 

  let most = function 
  | Empty -> raise Not_found
  | s -> most_aux (min s) s

  let rec least_aux (x,q) = function 
  | Empty -> (x,q)
  | Node {left; value; qty; right} -> 
      let (x,q) = if qty < q then (value, qty) else (x,q) in 
      let (lx, lq) = least_aux (x,q) left
      and (rx, rq) = least_aux (x,q) right in
      if lq < rq then (lx, lq) else (rx, rq)
  
  let least = function 
  | Empty -> raise Not_found
  | s -> least_aux (min s) s

  let rec fold_left f accu = function 
  | Empty -> accu
  | Node {left; value; qty; right} -> 
      let res = ref (fold_left f accu left) in 
      for _ = 1 to qty do res := f !res value done;
      fold_left f !res right

  let rec fold_right f s accu = match s with
  | Empty -> accu
  | Node{left; value; qty; right} -> 
      let res = ref (fold_right f right accu) in 
      for _ = 1 to qty do res := f value !res done;
      fold_right f left !res

  let rec for_all p = function
  | Empty -> true
  | Node{left; value; right} -> p value && for_all p left && for_all p right

  let rec exists p = function
  | Empty -> false
  | Node{left; value; right} -> p value || exists p left || exists p right

  let rec filter p = function
  | Empty -> Empty
  | (Node{left; value; qty; right}) as t ->
    (* call [p] in the expected left-to-right order *)
    let l' = filter p left in
    let pv = p value in
    let r' = filter p right in
    if pv then
      if left==l' && right==r' then t else create_ob l' (value, qty) r'
    else concat l' r'

  let rec filterm p = function 
  | Empty -> Empty
  | (Node {left; value; qty; right} as t) -> 
    let l' = filterm p left in
    let pv = p (value, qty) in
    let r' = filterm p right in
    if pv then 
      if left==l' && right==r' then t else create_ob l' (value, qty) r'
    else concat l' r'

  let rec partition p = function
  | Empty -> (Empty, Empty)
  | Node{left; value; qty; right} ->
    (* call [p] in the expected left-to-right order *)
    let (lt, lf) = partition p left in
    let pv = p (value, qty) in
    let (rt, rf) = partition p right in
    if pv
    then (create_ob lt (value, qty) rt, concat lf rf)
    else (concat lt rt, create_ob lf (value, qty) rf)

  let rec of_nodes = function 
  | (x, m) :: r -> add_many (x, m) (of_nodes r)
  | [] -> Empty

  let rec to_nodes_aux accu = function 
  | Empty -> accu
  | Node {left; value; qty; right} -> 
    let l= (value, qty) :: (to_nodes_aux accu right) in 
    to_nodes_aux l left

  let to_nodes = to_nodes_aux []  

  let choose s = fst (min s)

  let choose_opt = function Empty -> None | s -> Some (choose s)

  let rec nth s i = match s with
  | Empty -> raise Not_found
  | Node { left; right; value; qty; card } ->
    if i >= card then raise Not_found;
    let l = cardinal left in
    if i < l
    then nth left i
    else if i >= l && i < l+qty 
    then value
    else nth right (i - l - qty)

  let rec nth_node s i = match s with
  | Empty -> raise Not_found
  | Node { left; right; value; qty; card } ->
    let l = nodes left in
    if i < l
    then nth_node left i
    else if i = l 
    then (value, qty)
    else nth_node right (i - l - 1)

  (* SET OPERATIONS *)
  let rec union s1 s2 = 
  match (s1, s2) with
  | (Empty, t2) -> t2
  | (t1, Empty) -> t1
  | (Node{left=l1; value=v1; qty=q1; right=r1; depth=h1}, 
     Node{left=l2; value=v2; qty=q2; right=r2; depth=h2}) ->
      if h1 >= h2 
      then 
        if h2 = 1 then
          add_many (v2, q2) s1 
        else begin
          let ll2, nb1, rr2 = split v1 s2 in 
          let l12=union l1 ll2 in
          let r12=union r1 rr2 in
          create_ob l12 (v1, nb1 + q1) r12 
        end
      else
        if h1 = 1 
        then 
          add_many (v1, q1) s2 
        else begin
          let ll1, nb2, rr1 = split v2 s1 in
          let l12=union ll1 l2 in
          let r12=union rr1 r2 in
          create_ob l12 (v2, nb2 + q2) r12
        end

  let rec inter s1 s2 = match (s1, s2) with
  | (Empty, _) -> Empty
  | (_, Empty) -> Empty
  | (Node {left=l1; value=v1; qty=q1; right=r1}, t2) ->
     let l2, nb1, r2 = split v1 t2 in 
     if nb1=0 
     then concat (inter l1 l2) (inter r1 r2) 
     else create_ob (inter l1 l2) (v1, Stdlib.min nb1 q1) (inter r1 r2)

  (* Same as split, but compute the left and right subtrees
     only if the pivot element is not in the set.  The right subtree
     is computed on demand. *)

  type split_bis =
  | Found
  | NotFound of t * (unit -> t)

  let rec split_bis (x:elt) = function
  | Empty -> NotFound (Empty, (fun () -> Empty))
  | Node {left; value; qty; right} ->
    let c = X.compare x value in
    if c = 0 
    then Found
    else if c < 0
    then match split_bis x left with
    | Found -> Found
    | NotFound (ll, rl) -> 
        NotFound (ll, (fun () -> create_ob (rl ()) (value, qty) right))
    else match split_bis x right with
    | Found -> Found
    | NotFound (lr, rr) -> NotFound (create_ob left (value, qty) lr, rr)

  let rec disjoint s1 s2 = match (s1, s2) with
  | (Empty, _) | (_, Empty) -> true
  | (Node{left=l1; value=v1; right=r1}, t2) ->
    if s1 == s2 
    then false
    else match split_bis v1 t2 with
    | NotFound(l2, r2) -> disjoint l1 l2 && disjoint r1 (r2 ())
    | Found -> false

  let rec diff s1 s2 = match (s1, s2) with
  | (Empty, _) -> Empty
  | (t1, Empty) -> t1
  | (Node{left=l1; value=v1; qty=q1; right=r1}, t2) ->
    let l2, n2, r2 = split v1 t2 in
    if q1 <= n2 
    then concat (diff l1 l2) (diff r1 r2)
    else create_ob (diff l1 l2) (v1, q1-n2) (diff r1 r2)

  type enumeration = End | More of elt * int * t * enumeration

  let rec cons_enum s e = match s with
  | Empty -> e
  | Node{left; value; qty; right} -> cons_enum left (More(value, qty, right, e))

  let rec compare_aux e1 e2 = match (e1, e2) with
  |  (End, End) -> 0
  | (End, _)  -> -1
  | (_, End) -> 1
  | (More(v1, q1, r1, e1), More(v2, q2, r2, e2)) ->
    let c = X.compare v1 v2 in
    if c <> 0
    then c
    else
      if q1 <> q2 
      then Stdlib.compare q1 q2
      else compare_aux (cons_enum r1 e1) (cons_enum r2 e2)

  let compare s1 s2 =
    compare_aux (cons_enum s1 End) (cons_enum s2 End)

  let equal s1 s2 =
    compare s1 s2 = 0

  let rec subbag s1 s2 = match (s1, s2) with
  | Empty, _ -> true
  | _, Empty -> false
  | Node {left=l1; value=v1; qty=q1; right=r1}, 
    (Node {left=l2; value=v2; qty=q2; right=r2} as t2) ->
      let c = X.compare v1 v2 in
      if c = 0 && q1=q2
      then subbag l1 l2 && subbag r1 r2
      else if q1 <> q2 
      then false
      else if c < 0 
      then subbag (Node {left=l1; value=v1; qty=q1; right=Empty; depth=0; card=0}) l2 
           && subbag r1 t2
      else subbag (Node {left=Empty; value=v1; qty=q1; right=r1; depth=0; card=0}) r2 
           && subbag l1 t2

  let rand = function
  | Empty -> raise Not_found
  | Node {card} as b -> nth b (Random.int card)

  let rand_node = function
  | Empty -> raise Not_found
  | b -> let n = nodes b in 
         nth_node b (Random.int n) 

  let rec get_opt (x:elt) = function
  | Empty -> None
  | Node {left; value; qty; right} ->
    let c = X.compare x value in
    if c = 0
    then Some (value, qty)
    else get_opt x (if c < 0 then left else right)

  let get x b = match get_opt x b with 
  | Some node -> node
  | None -> raise Not_found

  let rec find_opt p = function
  | Empty -> None
  | Node {left; value; qty; right} ->
    if p (value, qty) 
    then Some (value, qty)
    else begin match find_opt p left with
    | None -> find_opt p right
    | Some (x, m) -> Some (x, m)
    end

  let find p b = match find_opt p b with 
  | Some node -> node
  | None -> raise Not_found

  let rec count p = function
  | Empty -> 0 
  | Node {left; value; qty; right} ->
     let l = count p left 
     and r = count p right in 
     if p value 
     then qty + l + r
     else l + r 

  let rec count_nodes p = function 
  | Empty -> 0
  | Node {left; value; qty; right} -> 
      let l = count_nodes p left
      and r = count_nodes p right in
      if p (value, qty) 
      then 1 + l + r
      else l + r

  let try_join l (v, q) r =
  (* [create_ob l v r] can only be called when (elements of l < v <
     elements of r); use [try_join l v r] when this property may
     not hold, but you hope it does hold in the common case *)
    if (l = Empty || X.compare (max l |> fst) v < 0)
      && (r = Empty || X.compare v (min r |> fst) < 0)
    then create_ob l (v, q) r
    else union l (add_many (v, q) r)

  let try_concat t1 t2 = match (t1, t2) with
  | (Empty, t) -> t
  | (t, Empty) -> t
  | (_, _) -> try_join t1 (min t2) (remove_min t2)

  let rec map f = function
  | Empty -> Empty
  | Node{left; value; qty; right} as t ->
    (* enforce left-to-right evaluation order *)
    let l' = map f left in
    let v' = f value in
    let r' = map f right in
    if left == l' && value == v' && right == r' 
    then t
    else try_join l' (v', qty) r'

  let rec umap f = function
  | Empty -> Empty
  | Node{left; value; qty; right} ->
    (* enforce left-to-right evaluation order *)
    let l' = ref (umap f left) in
    for _ = 1 to qty do l' := add (f value) !l' done;
    let r' = umap f right in
    try_concat !l' r'

  let rec mapm f = function
  | Empty -> Empty
  | Node{left; value; qty; right} as t ->
    (* enforce left-to-right evaluation order *)
    let l' = mapm f left in
    let (v', q') = f (value, qty) in
    let r' = mapm f right in
    if left == l' && value == v' && qty = q' && right == r' 
    then t
    else try_join l' (v', q') r'

  let rec filterm_map f = function
  | Empty -> Empty
  | Node {left; value; qty; right} as t ->
    (* enforce left-to-right evaluation order *)
    let l' = filterm_map f left in
    let v' = f (value, qty) in
    let r' = filterm_map f right in
    begin match v' with
    | Some (v', q') ->
      if left == l' && value == v' && qty == q' && right == r' 
      then t
      else try_join l' (v', q') r'
    | None -> try_concat l' r'
    end

  let of_sorted_list l =
    let rec sub n l = match n, l with
    | 0, l -> Empty, l
    | 1, x0 :: l -> singleton x0, l
    | 2, x0 :: x1 :: l ->
        if x0 = x1 
        then Node {left=Empty; value=x0; qty=2; right=Empty; depth=1; card=2},l
        else Node {left=singleton x0; value=x1; qty=1; right=Empty; depth=2; card=2}, l
    | 3, x0 :: x1 :: x2 :: l ->
        if x0 = x1 && x1 = x2 
        then Node {left=Empty; value=x0; qty=3; right=Empty; depth=1; card=3}, l
        else if x0 = x1 
        then Node {left=Empty; value=x0; qty=2; right=singleton x2; depth=2; card=3}, l
        else if x1 = x2 
        then Node {left=singleton x0; value=x1; qty=2; right=Empty; depth=2; card=3}, l
        else Node {left=singleton x0; value=x1; qty=1; right=singleton x2; depth=2; card=3}, l
    | n, l ->
      let nl = n / 2 in
      let left, l = sub nl l in
      match l with
      | [] -> assert false
      | mid :: l ->
        let right, l = sub (n - nl - 1) l in
        create_ob2 left (mid, 1) right, l
    in
    fst (sub (List.length l) l)

  let of_list = function 
  | [] -> empty
  | [x0] -> singleton x0
  | [x0; x1] -> add x1 (singleton x0)
  | [x0; x1; x2] -> add x2 (add x1 (singleton x0))
  | [x0; x1; x2; x3] -> add x3 (add x2 (add x1 (singleton x0)))
  | [x0; x1; x2; x3; x4] -> add x4 (add x3 (add x2 (add x1 (singleton x0))))
  | l -> of_sorted_list (List.sort X.compare l)

  let add_seq i m =
    Seq.fold_left (fun s x -> add x s) m i

  let of_seq i = add_seq i empty

  let rec seq_of_enum_ c () = match c with
  | End -> Seq.Nil
  | More (x, m, t, rest) -> 
    let ans=ref (seq_of_enum_ (cons_enum t rest)) in
    for _ = 1 to m do ans := fun () -> Seq.Cons (x, !ans) done;
    !ans()

  let to_seq c = seq_of_enum_ (cons_enum c End)

  let rec snoc_enum s e = match s with
  | Empty -> e
  | Node {left; value; qty; right} -> snoc_enum right (More(value, qty, left, e))

  let rec rev_seq_of_enum_ c () = match c with
  | End -> Seq.Nil
  | More (x, m, t, rest) -> 
    let ans=ref (rev_seq_of_enum_ (snoc_enum t rest)) in
    for _ = 1 to m do ans := fun () -> Seq.Cons (x, !ans) done;
    !ans()

  let to_rev_seq c = rev_seq_of_enum_ (snoc_enum c End)

  let to_seq_from (low:elt) s =
    let rec aux low s c = match s with
    | Empty -> c
    | Node {left; value; qty; right} ->
      begin match X.compare value low with
      | 0 -> More (value, qty, right, c)
      | n when n<0 -> aux low right c
      | _ -> aux low left (More (value, qty, right, c))
      end
      in seq_of_enum_ (aux low s End)

  let rec to_rset = function 
  | Empty -> Empty
  | Node ({left; right} as t) -> 
      Node {t with qty=1; left=to_rset left; right=to_rset right}
end

(* TESTS *)
(*
 
module N : (OrderedType with type t = int) = 
struct 
  type t = int 
  let compare = (-) 
  let to_string = string_of_int 
end

module IntBag = Make(N)

module IntBagBag = Make(IntBag)

let _= 
  let mintadd k s = IntBag.fold_left (+) 0 s + k in

  let props s = 
    if (IntBagBag.cardinal s <= 128) then IntBagBag.print_tree s;
    Printf.printf "Cardinal = %d\n" (IntBagBag.cardinal s);
    Printf.printf "#node = %d\n" (IntBagBag.nodes s);
    Printf.printf "Sum of elements = %d\n\n" (IntBagBag.fold_left mintadd 0 s);
  in
  Random.self_init ();

  let n=1000
  and super = ref IntBagBag.empty in 

  for k = 0 to n-1 do
    let s=ref IntBag.empty in
    for i = 0 to n-1 do s := MInt.add (Random.int n) !s done;
    super := IntBagBag.add !s !super
  done;

  props !super
*)
