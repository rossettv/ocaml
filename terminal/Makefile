SHELL=/bin/bash
BINDIR=$(HOME)/bin
MANDIR=$(HOME)/local/man
SRCDIR=$(HOME)/src
LIBDIR=$(SRCDIR)/lib
IMPORT=$(LIBDIR)
EXTLIB=
AUTOLIBS=unix str
TEXHIDES=-hide Unix -hide Graphics
OPT=ocamlopt.opt -labels -I $(LIBDIR) $(AUTOLIBS:%=-I +%) 
CC=ocamlc.opt -labels -I $(LIBDIR) $(AUTOLIBS:%=-I +%) 

GRAPHICS=$(HOME)/.opam/5.0.0/lib/graphics
OCAMLDEP=ocamldep $(AUTOLIBS:%=-I +%)
OCAMLDOC=ocamldoc -sort -I $(LIBDIR) $(HIDES) $(AUTOLIBS:%=-I +%)
OCAMLHTML=$(OCAMLDOC) -html -charset utf8 -colorize-code 
OCAMLTEX=$(OCAMLDOC) -latex -charset utf8 -colorize-code $(TEXHIDES)
OCAMLMAN=$(OCAMLDOC) -man -man-mini

OCAMLOPT=$(OPT) $(EXTLIB:%=%.cmxa)
OCAMLC=$(CC) $(EXTLIB:%=%.cma) 

MODULE:=terminal
PACKAGE:=terminal
AR=$(MODULE:%=%.a)
CMXA=$(MODULE:%=%.cmxa)
CMA=$(MODULE:%=%.cma)
CMI=$(MODULE:%=%.cmi)
CMO=$(MODULE:%=%.cmo)
CMX=$(MODULE:%=%.cmx)
MLI=$(MODULE:%=%.mli)
OBJ=$(MODULE:%=%.o)
SRC=$(MODULE:%=%.ml)

ARCHIVE:=$(PACKAGE:%=%.a)
PACKA:=$(PACKAGE:%=%.cma)
PACKX:=$(PACKAGE:%=%.cmxa)

TEX=$(PACKAGE:%=%.tex)
PDF=$(PACKAGE:%=%.pdf)

.SUFFIXES: .ml .mli .cmi .cmo .cmx .cma .cmxa .o .a .tex .pdf .log .aux 

all:
	make $(PACKA) $(ARCHIVE) $(PACKX) man 

install:
	mv -f $(PACKA) $(ARCHIVE) $(PACKX) $(OBJ) $(CMI) $(CMO) $(CMX) $(LIBDIR)
	mv -f $(shell MODULE=$(MODULE); echo $${MODULE^}).3o $(MANDIR)/man3

$(ARCHIVE): $(PACKA)
	$(OCAMLC) -a -o $@ $(OBJ)

$(PACKA): $(CMO)
	$(OCAMLC) -a -o $@ $(CMO)

$(PACKX): $(CMX)
	$(OPT) -a -o $@ $(CMX)

.o.a: 
	$(OCAMLC) -a -o $@ $<

.cmx.cmxa: 
	$(OCAMLOPT) -a -o $@ $< 

.cmo.cma: 
	$(OCAMLC) -a -o $@ $< 

.ml.mli: 
	$(OCAMLC) -i $< > $@

.mli.cmi: 
	$(OCAMLC) -c -o $@ $<

.ml.cmo: 
	$(OCAMLC) -c -o $@ $<

.ml.cmx: 
	$(OCAMLOPT) -c -o $@ $<

.ml.o: 
	$(OCAMLOPT) -o $@ $<

.ml.tex: 
	$(OCAMLDOC) -latex -o $@ $<

.ml.man:
	$(OCAMLMAN) -o $@ $<

.tex.pdf:
	@sed 's/usepackage\[latin1\]{inputenc}/usepackage\[utf8\]{inputenc}/' $< \
	  > $@.tmp
	@mv -f $@.tmp $@
	pdflatex $<

$(PDF): $(SRC)
	$(OCAMLTEX) -o doc.tex $(SRC)
	make doc.pdf
	mv doc.pdf $@

man:
	$(OCAMLMAN) $(SRC)

doc:
	make $(PDF) man
	
clean:
	rm -f *.mli *.cmi *.cmo *.cmx *.cma *.cmxa *.a *.o *.tex *.log *.aux *.bin

.depend: $(SRC)
	ocamldep $(SRC) > $@
