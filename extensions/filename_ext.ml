(** {2 [Filename] extensions} *)

(** A [path] is a list of directory names *)
type path = string list

(** {2 Handling of filenames of the form [directory/name-index.ext]} *)

(** The type [Filename.indexed] covers filenames of the type 
    {ul {- [directory/name]} 
        {- [directory/name.ext] }
        {- [directory/name-index] }
        {- [directory/name-index.ext] }}
    that is the [directory] and the [name] are mandatory, whereas
    [index] and [ext]ension are optional. The indices are non-negative integers.
*)
type indexed = { 
  directory : string;
  name : string;
  index : int option;  
  extension : string option
}

(**/**)
(** [get_name_index s] return the name and the index 
    of a string of a form [name-index] or [name_index] *)
let get_name_index name =
  let k=ref (String.length name - 1) in 
  while Char_ext.is_digit name.[!k] && !k > 0 do decr k done;
  let idx = 
    try
      if name.[!k] = '-' || name.[!k] = '_' 
      then Some (int_of_string (String_ext.tl name !k)) 
      else None
    with Failure _ -> None 
  in
  (name, idx)
(**/**)
  
(** [indexed fname] returns a [Filename.indexed] record decomposing 
    the filename into directory, name, index, extension *)
let from_name ?(extension="") fname = 
  let directory = Filename.dirname fname
  and bname, extension = 
    let b = Filename.basename fname |> Filename.remove_extension in 
    match Filename.extension fname with
    | "" -> (b, if extension = "" then None else Some extension) 
    | s -> 
      let b' = String.sub b 0 (String.length b - String.length s) in 
      match String_ext.end_float fname with 
      | None -> (b', Some s ) 
      | Some _ -> (b, if extension = "" then None else Some extension) 
  in 
  let (name, index) = get_name_index bname in
  { directory; name; index; extension}

(**/**)
(** If [indexed] is false then [compatible indexed name s] is true only if [name=s].
    Otherwise, if [indexed] is [true], [compatible indexed name s] is true
    if [s] is of the form [name-index] or [name_index] where [index] is an integer. *)
let compatible indexed bname s = 
  let l=String.length bname 
  and n=String.length s in
  (bname = s) || 
  (indexed 
    && n > l
    && String.sub s 0 l = bname
    && (s.[l] = '-' || s.[l] = '_') 
    && String_ext.tl s l |> String_ext.is_integer)

(**/**)
(** [find_all name ext_list path] returns a list of [Filename.indexed] containing
    all files in [path] matching the query, in no particular order.
    The flag [indexed] is [false] by default, in this case, only files with
    the given [name] are compatible with the query. 
    If [indexed] is [true], files with an index are also compatible.

    The name can be given without or without extension.
    If it is given with extension, only this extension is searched 
    and the argument [ext_list] is ignored. Otherwise, when [name] 
    hasno extensions, all extensions in [ext_list] are searched. 
    If [ext_list] is empty and [name] has no 
    extensions, only file without extension are searched. 

    If there is no match, it returns an empy list. *)
let find_all ?(indexed=false) name ext_list (path:path) =
  let bname = Filename.remove_extension name 
  and elist = match Filename.extension name with
    | "" -> 
       if ext_list = [] 
       then [""] 
       else List.map (fun s -> if s.[0] = '.' then s else "."^s) ext_list
    | s  -> [s] 
  and dlist =
    if path=[] then ["."] else path 
  and ans : indexed list ref = ref []
  in 
  
  List.iter (fun dir -> 

    let file = Sys.readdir dir in
    for i=0 to Array.length file - 1 do
      if compatible indexed bname file.(i) 
         && List.mem (Filename.extension file.(i)) elist 
      then ans := from_name file.(i) :: !ans
    done
  ) dlist;
  !ans

(** [find_opt name ext_list path] return a [Filename.indexed option] object.
    The value [None] is returned if no file match the query. Otherwise one
    object is returned. In the case where several files match the query, 
    it is not specified which one is returned by [find_opt] *)
let find_opt ?(indexed=false) name ext_list path = 
  match find_all ~indexed name ext_list path with
  | [] -> None
  | x :: _ -> Some x

(** [find] is the same as [find_opt] but it returns a matching [Filename.indexed]
    of raises [Not_found]. *)
let find ?(indexed=false) name ext_list path =
  match find_all ~indexed name ext_list path with
  | [] -> raise Not_found
  | x :: _ -> x

(** [to_string ~w indexed] returns a string with the directory, name, index and extension.
    The index is padded with zeros such that it is made of [w] characters.
    Negative index are ignored. *)
let to_string ?(w=1) indexed = 
  let e = match indexed.extension with
  | None -> "" 
  | Some ext -> 
     if String.length ext > 0 && ext.[0] != '.' 
     then "."^ext 
     else ext 
  in
  match indexed.index with 
  | None -> Printf.sprintf "%s/%s%s" indexed.directory indexed.name e
  | Some n -> 
    if n < 0 
    then Printf.sprintf "%s/%s%s" indexed.directory indexed.name e
    else Printf.sprintf "%s-%0*d%s" indexed.name (max (Int_ext.digits n) w) n e

(**/**)
(** [addzero name] returns the name where the index has an extra zero. *)
let addzero fname =
  let ind = from_name fname in 
  match ind.index with
  | None -> to_string { ind with index = Some 0 }
  | Some n -> 
      let d = Int_ext.digits n in 
      to_string ~w:(d+1) ind
(**/**)

(** [padding name ext_list path] renames all files found by the search
    [find_all ~indexed:true name ext_list path] by adding zeroes such 
    that all indices have the same number of digits. 
    Non-indexed files are not affected. *)
let padding name ext_list path =
  let files = find_all ~indexed:true name ext_list path in
  let n = 
    List.map (fun f -> match f.index with None -> 0 | Some k -> k) files 
    |> List.fold_left max 0 
  in 
  let w = Int_ext.digits n + 1 in

  List.iter (fun f -> Sys.rename (to_string f) (to_string ~w f)) files

(** [unused_index path name ext_list] returns the 
    lowest unused index in the path for basename [name] with 
    any of the extensions in the [ext_list]. *)
let unused_index name ext_list path = 
  let files = find_all ~indexed:true name ext_list path in
  let n = 
    List.map (fun f -> match f.index with None -> -1 | Some k -> k) files 
    |> List.fold_left max 0 
  in n+1

(** [next_file indexed] is the [Filename.indexed] description of a non-existing
    file in the same directory as [indexed], with the same base name and 
    extension and the lowest value of unused index. *)
let next_file ind = 
  let n = match ind.extension with
  | None -> unused_index ind.name [] [] 
  | Some ext -> unused_index ind.name [ext] [] 
  in 
  { ind with index = Some n }
