(** Mathematical constants *)

(** {2 Usual square roots} *)
(** The number {%latex:$\sqrt2$%} *)
let sqrt2=sqrt 2.
(** The number {%latex:$\sqrt3$%} *)
and sqrt3=sqrt 3. 
(** The number {%latex:$\sqrt5$%} *)
and sqrt5=sqrt 5. 

(** {2 Other algebraic constants} *)
(** The golden number is the largest root of {%latex:$x^2=x+1$%}. *)
let gold =(Stdlib.sqrt 5. +. 1.) *. 0.5
(** The plastic number is the real root of {%latex:$x^3=x+1$%}. *)
and plastic =  (0.5 +. (Stdlib.sqrt 69.) /. 18.) ** (1. /. 3.) 
            +. (0.5 -. (Stdlib.sqrt 69.) /. 18.) ** (1. /. 3.)
(** The silver number is the largest root of {%latex:$x^2=2x+1$%}. *) 
and silver = Stdlib.sqrt 2. +. 1.
(** The bronze number is the largest root of {%latex:$x^2=3x+1$%}. *)
and bronze = (Stdlib.sqrt 13. +. 3.) *. 0.5
(** The copper number is the largest root of {%latex:$x^2=4x+1$%}. *)
and copper = Stdlib.sqrt 5. +. 2.
(** The nickel number is the largest root of {%latex:$x^2=5x+1$%}. *)
and nickel = (Stdlib.sqrt 29. +. 5.) *. 0.5
(** The stain number is the largest root of {%latex:$x^2=6x+1$%}. *)
and stain = Stdlib.sqrt 10. +. 3. 

(** {2 Archimedes' constant {%latex:$\pi$%}} *)
(** The value of {%latex:$\pi$%}. *)
let pi=Float.pi
(** The number {%latex:$2\pi$%} *)
let two_pi=pi *. 2.
(** The number {%latex:$4\pi$%} *)
and four_pi=pi *. 4.
(** The number {%latex:$\frac\pi2$%} *) 
and half_pi=pi *. 0.5
(** The number {%latex:$\frac\pi3$%} *) 
and third_pi=pi /. 3. 
(** The number {%latex:$\frac\pi4$%}. *)
and fourth_pi=pi *. 0.25
(** The number {%latex:$\sqrt\pi$%} *)
let sqrt_pi=Stdlib.sqrt pi
(** The number {%latex:$\sqrt{2\pi}$%} *)
let sqrt_two_pi = sqrt_pi *. sqrt2
(** The number {%latex:$\frac12\sqrt\pi$%} *)
let half_sqrt_pi= sqrt_pi *. 0.5

(** {2 Euler constants} *)
(** [euler] = {%latex:$\mathrm e=\exp(1)$%}. *)
let euler=Stdlib.exp 1. 
(** [mascheroni] = {%latex:$\gamma$%}. The Euler-Mascheroni constant
  is the limit of {%latex:$\ln n -\sum_{k=1}^n\frac1k$%} as
  {%latex:$n\to\infty$%}. *)
and mascheroni = 0.5772156649015328606

(** {2 Usual logarithms} *)
(** The number {%latex:$\ln2$%} *)
let ln2=Stdlib.log 2. 
(** The logarithm of 10 in logarithmic base 2, {%latex:$\log_2(10)$%} *)
let lg10=Stdlib.log 10. /. ln2

(** {2 Zeta function} *)
(** [apery] = {%latex:$\zeta(3)$%} is {%latex:Ap\'ery%}'s constant. *)
let apery = 1.2020569031595942854 (* zeta(3) *)

(** {2 Chaos theory} *)
(** Feigenbaum's constant in dynamic systems. This
    number is not known to a great accuracy. *)
let feigenbaum = 4.6692 
