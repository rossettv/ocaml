(* Fichier d'interface entre les fonctions définies dans 
   include/posix_external.c et les programmes OCaml *)

external allocate : 
  out_channel -> int -> int -> unit = "caml_posix_fallocate"

let alloc_open_out name size=
  let channel=open_out name in
  allocate channel 0 size;
  channel
