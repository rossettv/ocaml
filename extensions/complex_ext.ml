(** Extensions of the [Complex] module *)
(** {2 Operations on real and imaginary parts} *)
(** Conversions from [float]. *)
type t = Complex.t
let of_float x : t = {re=x; im=0.}
and imaginary y : t = {re=0.; im=y}
(** Conversion from [int] *)
let of_int n = of_float (Float.of_int n)
(** Multiplication by the imaginary unit [Complex.i]. *)
let imul (z : t) : t = { re= -. z.im; im= z.re}
(** Division by the imaginary unit [Complex.i]. This is 
    equivalent to a multiplication by [-i].  *)
let idiv (z : t) : t = { re= z.im; im= -. z.re}

(** {2 Shortcuts for operators} *)
(** Addition *)
let ( +: ) : t -> t -> t = Complex.add
(** Multiplication *)
and ( *: ) : t -> t -> t = Complex.mul
(** Substraction *)
and ( -: ) : t -> t -> t = Complex.sub
(** Unary negative *)
and (~-: ) : t -> t = Complex.neg
(** Division *)
and ( /: ) : t -> t -> t = Complex.div
(** Power *)
and ( **: ) : t -> t -> t = Complex.pow

(** {2 Constants} *)
(**)
let pi = of_float Float.pi
let ipi = imul pi
let sqrttwopi = of_float (Stdlib.sqrt (Const_ext.two_pi))
let mascheroni = of_float Const_ext.mascheroni

(** {2 Elementary functions} *)
(** The complex exponential {%latex:$\mathrm{e}^{\mathrm{i} x}$%}, where
   {%latex:$x$%} is a real number. *)
let expi x : t = Complex.exp (imaginary x)
(** The complex exponential {%latex:$\mathrm{e}^{\mathrm{i}\pi x}$%}, where
   {%latex:$x$%} is a real number. *)
let eipi x : t = Complex.exp (imaginary (Float.pi *. x))
(** The complex hyperbolic sinus. *)
let sinh (z : t) = let ez = Complex.exp z in 
  (ez -: (Complex.one /: ez)) *: (of_float 0.5)
(** The complex hyperbolic cosinus. *)
let cosh (z : t) = let ez = Complex.exp z in 
  (ez +: (Complex.one /: ez)) *: (of_float 0.5)
(** The complex sinus. *)
let sin z = idiv (sinh (imul z)) 
(** The complex cosinus. *)
let cos z = cosh (imul z)
(** Lexicographic comparison *)
let compare : t -> t -> int = Stdlib.compare
