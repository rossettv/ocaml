(** {2 [Stack] extensions} *)

type 'a t = 'a Stack.t

(** [Stack.to_list s] is a list of the elements of [s] in the same
    order as in [s]. *)
let rec to_list (stack : 'a t) = 
  if Stack.is_empty stack 
  then []
  else (Stack.pop stack) :: (to_list stack)

(** [Stack.drop s] removes the first element of [s]. Raises 
    [Failure "drop : stack error"] if [s] is empty. 

 @note : implemented in OCaml 5.1.0
         raises Stdlib.Stack.Empty instead of Failure "..." *)
let drop (stack : 'a t) = begin
  try let _ = Stack.pop stack in ()
  with Stack.Empty -> failwith "drop : empty stack"
     | _ -> failwith "drop : stack error"
  end


