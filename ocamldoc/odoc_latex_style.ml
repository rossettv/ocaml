(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Maxence Guesdon, projet Cristal, INRIA Rocquencourt        *)
(*                                                                        *)
(*   Copyright 2001 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** The content of the LaTeX style to generate when generating LaTeX code. *)

(* Modified : VR 23/10/13 *)
(* Added lstlisting support and ocaml style *)

let content ="
%% Support macros for LaTeX documentation generated by ocamldoc.\
\n%% This file is in the public domain; do what you want with it.\
\n\
\n\\NeedsTeXFormat{LaTeX2e}\
\n\\ProvidesPackage{ocamldoc}\
\n              [2001/12/04 v1.0 ocamldoc support]\
\n\
\n\\usepackage{lmodern}\
\n\\usepackage{color}\
\n\\usepackage[usenames,svgnames]{xcolor}\
\n\\usepackage{listings}\
\n\
\n%%%%%%%%%%%%%%%%%%%%%%%%% Syntax highlighting %%%%%%%%%%%%%%%%%%%%%\
\n%%%%% COULEURS \
\n%%%%% Compatibles tous types de daltonisme\
\n% http://jfly.iam.u-tokyo.ac.jp/color/#pallet\
\n\\definecolor{noir}{RGB}{0,0,0}\
\n%\\definecolor{orange}{RGB}{230,159,0}\
\n\\definecolor{orange}{RGB}{185,127,0}\
\n%\\definecolor{bleuciel}{RGB}{86,180,233}\
\n\\definecolor{bleuciel}{RGB}{65,143,183}\
\n%\\definecolor{vertbleu}{RGB}{0,158,115}\
\n\\definecolor{vertbleu}{RGB}{0,117,85}\
\n%\\definecolor{jaune}{RGB}{240,228,66}\
\n\\definecolor{jaune}{RGB}{240,228,66}\
\n%\\definecolor{bleu}{RGB}{0,114,178}\
\n\\definecolor{bleu}{RGB}{0,94,147}\
\n%\\definecolor{vermillon}{RGB}{213,94,0}\
\n\\definecolor{vermillon}{RGB}{167,74,0}\
\n%\\definecolor{violet}{RGB}{204,121,167}\
\n\\definecolor{violet}{RGB}{167,72,125}\
\n\
\n% Style des langages \
\n% style par défaut\
\n\\def\\dfstyle{\\fontseries{n}\\selectfont\\color{noir}}\
\n% syntaxe (coupure de ligne, indentations)\
\n\\def\\sxstyle{\\fontseries{b}\\selectfont\\color{vermillon}}\
\n%\\def\\sxstyle{\\fontseries{b}\\selectfont\\color{Brown}}\
\n% = signe égal (affectation seulement)\
\n\\def\\eqstyle{\\fontseries{m}\\selectfont\\color{violet}}\
\n% opérations\
\n\\def\\opstyle{\\fontseries{b}\\selectfont\\color{orange}}\
\n% comparateurs (dont ==)\
\n\\def\\cpstyle{\\fontseries{m}\\selectfont\\color{bleu}}\
\n%\\def\\cpstyle{\\fontseries{m}\\selectfont\\color{DarkBlue}}\
\n% descripteurs\
\n\\def\\dsstyle{\\fontseries{m}\\selectfont\\color{vertbleu}}\
\n%\\def\\dsstyle{\\fontseries{m}\\selectfont\\color{DarkSlateGray}}\
\n% mots-clés\
\n\\def\\kwstyle{\\fontseries{b}\\selectfont\\color{bleu}}\
\n%% \\def\\kwstyle{\\fontseries{b}\\selectfont\\color{DarkBlue}}\
\n% commentaires\
\n\\def\\cmstyle{\\fontseries{m}\\selectfont\\color{vertbleu}}\
\n% nombres\
\n\\def\\nbstyle{\\fontseries{m}\\selectfont\\color{bleu}}\
\n% numéros de ligne\
\n\\def\\nostyle{\\tiny\\color{vermillon}}\
\n%\\def\\nostyle{\\tiny\\color{DarkRed}}\
\n% chaîne de caractères\
\n\\def\\ststyle{\\fontseries{m}\\selectfont\\color{violet}} \
\n%\\def\\ststyle{\\fontseries{m}\\selectfont\\color{Purple}} \
\n% identificateurs (nom des variables, des fonctions...)\
\n\\def\\idstyle{\\fontseries{m}\\selectfont}\
\n% directive d'importation \
\n\\def\\imstyle{\\fontseries{b}\\selectfont\\color{orange}}\
\n%\\def\\imstyle{\\fontseries{b}\\selectfont\\color{Sienna}}\
\n% modules \
\n\\def\\mdstyle{\\fontseries{b}\\selectfont\\color{violet}}\
\n% modules types \
\n\\def\\mdstyle{\\fontseries{b}\\selectfont\\color{orange!50!black}}\
\n% erreur (exceptions)\
\n\\def\\xcstyle{\\fontseries{m}\\selectfont\\color{red!50!black}}\
\n% booléens\
\n\\def\\tfstyle{\\fontseries{b}\\selectfont\\color{bleuciel!35!black}}\
\n\
\n% Options de listings \
\n\\lstloadlanguages{[Objective]Caml}\
\n\\lstset{%\
\n  basicstyle=\\ttfamily\\small,\
\n  mathescape=false,\
\n  breaklines=true,\
\n  xleftmargin=\\parindent,\
\n  xrightmargin=0pt, %\\parindent,\
\n  breakatwhitespace=true,\
\n  linewidth=\\linewidth,\
\n  % Placer des commandes LaTeX en fin de ligne, par exemple #* \\label{code:loop}\
\n  %escapeinside={##£}{\\^^M}, \
\n  escapechar={£},\
\n  showstringspaces=false\
\n  inputencoding=utf8,\
\n  keepspaces=true,\
\n  frame={},\
\n  framerule=0.1pt,\
\n  framexleftmargin=0.5em,\
\n  framesep=3mm,\
\n  columns=flexible,\
\n  backgroundcolor=,\
\n  emptylines=1,\
\n  % Style commun pour tous les langages (python, octave, MATLAB)\
\n  basicstyle=\\ttfamily\\fontseries{l}\\selectfont,\
\n  identifierstyle=\\idstyle,\
\n  numbers=left,\
\n  numberstyle=\\nostyle,\
\n  keywordstyle=\\kwstyle,\
\n  commentstyle=\\cmstyle, \
\n  stringstyle=\\ststyle,\
\n  aboveskip=0pt, %-0.25em,\
\n  belowskip=0pt, %-0.5em,\
\n  breakindent=4em,\
\n  breaklines=true,\
\n  breakatwhitespace=true,\
\n  xleftmargin=1em\
\n}\
\n\
\n\\lstdefinestyle{ocaml}{%\
\n  language=caml,\
\n  literate=*{+}{{\\opstyle+}}1\
\n            {+.}{{\\opstyle+}{\\opstyle.}}2\
\n            {-}{{\\opstyle-}}1\
\n            {-.}{{\\opstyle-}{\\opstyle.}}2\
\n            {*}{{\\opstyle*}}1\
\n            {*.}{{\\opstyle*}{\\opstyle.}}2 \
\n            {/}{{\\opstyle/}}1\
\n            {/.}{{\\opstyle/}{\\opstyle.}}2 \
\n            {\\^}{{\\opstyle\\^{}}}1\
\n            {\\^\\^}{{\\opstyle\\^\\^{}}}2\
\n            {->}{{\\opstyle$\\rightarrow$}}2\
\n            {<-}{{\\eqstyle$\\leftarrow$}}2\
\n            {:}{{\\dsstyle:}}1    \
\n            {:=}{{\\eqstyle:}{\\eqstyle=}}2\
\n            {!}{{\\kwstyle!}}1\
\n            {=}{{\\eqstyle=}}1\
\n            {>}{{\\cpstyle>}}1\
\n            {<}{{\\cpstyle<}}1\
\n            {==}{{\\cpstyle=}{\\cpstyle=}}2\
\n            {>=}{{\\cpstyle>}{\\cpstyle=}}2\
\n            {<=}{{\\cpstyle<}{\\cpstyle=}}2\
\n            {\\{\\\\textquotesingle\\}}{{\\dsstyle'}}1\
\n            {\\{\\\\textquotesingle\\}a}{{\\dsstyle$\\alpha$}}1\
\n            {\\{\\\\textquotesingle\\}b}{{\\dsstyle$\\beta$}}1\
\n            {\\{\\\\textquotesingle\\}c}{{\\dsstyle$\\gamma$}}1\
\n            {\\{\\\\textquotesingle\\}d}{{\\dsstyle$\\delta$}}1\
\n            {\\{\\\\textquotesingle\\}e}{{\\dsstyle$\\epsilon$}}1\
\n            {\\{\\\\textquotesingle\\}f}{{\\dsstyle$\\varphi$}}1\
\n            {\\}\\}\\{\\\\tt\\{\\ =\\ \\}\\}}{{\\mdstyle\\ =\\ }}3\
\n            {()}{{\\dsstyle{()}}}2,\
\n  morekeywords={val,let,rec,in,and,for,to,downto,done,match,with,while,do,begin,fun,function},\
\n  emphstyle=[2]\\imstyle,   % couleur des directives d'importation / foncteurs\
\n  emph=[2]{open,include,inherit,external,Make,functor},\
\n  emphstyle=[3]\\mdstyle,   % couleur des modules\
\n  emph=[3]{Stdlib,Array,List,String,Regexp,Str,Seq,Set,Bytes,,Map,Hashtbl},\
\n  emphstyle=[4]\\dsstyle,   % couleur des types\
\n  emph=[4]{unit,of,bool,int,float,string,option,ref,list,array,lazy,constraint,nonrec},      % \
\n  emphstyle=[5]\\nbstyle,   % couleur des pseudo-nombres\
\n  emph=[5]{nan,inf},\
\n  emphstyle=[6]\\xcstyle,   % couleur des exceptions\
\n  emph=[6]{exception,Not_found,Failure,Error,End_of_file,try,or},  % or is deprecated \  
\n  emphstyle=[7]\\tfstyle,\
\n  emph=[7]{true,false,assert,lor,lxor,land,not,if,then,else,when},\
\n  emphstyle=[8]\\mtstyle,\
\n  emph=[8]{module,struct,sig,end,class,object,private,method,new,initializer,virtual},\
\n  emphstyle=[9]\\opstyle,\
\n  emph=[9]{asr,lsl,lsr,mod,as},\
\n}\
\n\
\n% Affichage du contenu d'un fichier de code\
\n% filename par egreg sur tex.SE\
\n% https://tex.stackexchange.com/a/325052/43020\
\n\\DeclareRobustCommand{\\filename}[1]{%\
\n  \\begingroup\
\n    % \\lstname seems to change hyphens into \\textendash\
\n    \\def\\textendash{-}%\
\n    \\filename@parse{#1}%\
\n    \\edef\\filename@base{\\detokenize\\expandafter{\\filename@base}}%\
\n    \\texttt{\\filename@base.\\filename@ext}%\
\n  \\endgroup\
\n}\
\n\
\n\\lstset{style=ocaml}\
\n\
\n\\lstnewenvironment{ocamldoccode}[1][numbers=none]{\\lstset{style=ocaml,#1}\\noindent}{}\
\n\
\n\\newenvironment{ocamldocdescription}\
\n{\\list{}{\\rightmargin0pt \\topsep0pt}\\raggedright\\item\\noindent\\relax\\ignorespaces}\
\n{\\endlist\\medskip}\
\n\
\n\\newenvironment{ocamldoccomment}\
\n{\\list{}{\\leftmargin 2\\leftmargini \\rightmargin0pt \\topsep0pt}\\raggedright\\item\\noindent\\relax}\
\n{\\endlist}\
\n\
\n\\let \\ocamldocparagraph \\paragraph\
\n\\def \\paragraph #1{\\ocamldocparagraph {#1}\\noindent}\
\n\\let \\ocamldocsubparagraph \\subparagraph\
\n\\def \\subparagraph #1{\\ocamldocsubparagraph {#1}\\noindent}\
\n\
\n\\let\\ocamldocvspace\\vspace\
\n\
\n%\\newenvironment{ocamldocindent}{\\list{}{}\\item\\relax}{\\endlist}\
\n\\newenvironment{ocamldocsigend}\
\n   {\\noindent\\qquad\\lstinline[style=ocaml]|sig|\
\n    \\quote\\vskip-2em\
\n\
\n    \\noindent}\
\n   {\\endquote\\vskip -1em\
\n\
\n    \\noindent\\qquad\\lstinline[style=ocaml]|end|\\smallskip}\
\n  \
\n\\newenvironment{ocamldocobjectend}\
\n   {\\noindent\\qquad\\lstinline[style=ocaml]|object|\
\n    \\quote\\vskip-2em\
\n\
      \\noindent}\
\n   {\\endquote\\vskip -1em\
\n    \\noindent\\qquad\\lstinline[style=ocaml]|end|\\smallskip}\
\n\
\n\\endinput"
