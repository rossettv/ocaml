(** [List] extensions *)

open Stdlib_ext 

(** [split default l] returns a pair made of the first element of [l] and its tail
    ([List.hd l, List.tl l]) is [l] is not empty and 
    [(default, [])] if [l] is empty. *)
let split default = function 
| x :: r -> (x, r)
| []     -> (default, [])

(** {2 List of [option] elements} *)
(** [squeeze l] returns the list of elements of [l] 
    that are equal to [Some x]. The result is therefore 
    shorter than the input list.  *)
let rec squeeze = function 
| None :: r -> squeeze r
| Some x :: r -> x :: (squeeze r)
| [] -> []

(** {2 List extractions} *)
(** [remove_first x l] returns a list where the first occurence of [x]
    has been removed. *)
let rec remove_first x = function
| y :: r -> if x = y then r else y :: (remove_first x r)
| [] -> []

(** [replace_first x y l] returns a list where the first occurence of [x]
    has been replace by [y]. *)
let rec replace_first x y = function 
| z :: r -> if z = x then y :: r else z :: (replace_first x y r)
| [] -> []

(** [tail n l] returns the list l with its [n] first elements removed. *)
let rec tail n = function 
  | x :: r as l -> if n > 0 then tail (n-1) r else l
  | [] -> if n=0 then [] else failwith "List.tail"

(** [last l] returns the last element of [l]. Raises [Not_found] if the
    list is empty. *)
let rec last = function 
  | x :: [] -> x 
  | x :: r -> last r
  | [] -> raise Not_found 

(** [filter_out p l] filters out the element satisfying the predicate [p].
    It returns the elements of [l] that would removed by [List.filter]
    and vice-versa. *)
let filter_out p = List.filter (p >| not)

(** [filteri p l] filters the elements of [l] that follow the predicate [p]
    where the position in the list is taken into account *)
let filteri p l =
  let rec aux i = function
  | x :: r -> if p i x then x :: (aux (i+1) r) else aux (i+1) r
  | [] -> [] 
  in aux 0 l 

(* [drop p l] is the list [l] where elements satisfying 
    the predicate [p] have been dropped. *)
(* let drop p = List.filter (fun x -> not (p x)) *)

(** {2 List of functions} *)
(** [map_list f_list x_list] applies all function in [f_list] 
    to all elements of the list [x_list]. The resulting list is ordered as 
    [[ f0 x0; f1 x0; ...; f0 x1; f1 x1; ...]]. *)
let rec map_list f_list = function 
| x :: r -> List.map ((|>) x) f_list @ (map_list f_list r)
| [] -> []

(** {2 List properties} *)
(** [count f l] counts the number of elements of [l]
   satisfying the predicate [f] (i.e. for which [f x] returns [true]. *)
let rec count f l =
  let n=ref 0 in List.iter (fun x -> if f x then incr n) l;
  !n

(** [min l] returns the minimum element of list [l] using [Stdlib.min].*)
let min = function
| x::l -> List.fold_left min x l
| [] -> failwith "List.min"

(** [max l] returns the maximum element of list [l] using [Stdlib.max].*)
let max = function
| x::l -> List.fold_left max x l
| [] -> failwith "List.max"

(** {2 [List] and [String]} *)
(** [to_string char_list] returns a string made of the chars in [char_list]. *)
let rec to_string = function 
| c :: r -> (String_ext.of_char c)^(to_string r)
| [] -> ""

(** [of_string s] returns the list of all characters in [s]. *)
let rec of_string = function 
| "" -> []
| s -> s.[0] :: of_string (String_ext.tl s 1)

(** {2 Set-like operations} *)
(** [inter l1 l2] is the list of elements of [l1] 
 *  belonging to [l2] as well. *)
let inter l = List.filter (fun x -> List.mem x l) 

(** [remove l1 l2] is the list [l2] without members of the list [l1] *)
let remove l1 l2 = filter_out (fun x -> List.mem x l1) l2

(** [sub l1 l2] is the list [l1] without members of the list [l2].
    This is an alias for [remove]. *)
let sub l1 l2 = remove l2 l1

(** {2 Zipping and unzipping} *)
(** [zip l1 l2] returns a list of pairs, where the first and second elements
    are the members of [l1] and [l2] respectively *)
let zip l1 l2 = List.map2 (fun x y -> (x, y)) l1 l2

(** [unzip l] returns two lists made of the first and second elements
    of members of [l]. *)
let rec unzip = function 
| (x, y) :: r -> 
    let (lx, ly) = unzip r in 
    (x :: lx, y :: ly)
| _ -> ([], [])

(** {2 Recursive constructor} *)
(** When the function [f] produces The command [chain f p x0] applies [f] to starting element [x0].
    The result is [f x0 = (y0, x1)] and [x1] is applied to [f]. The process is repeated
    until [p x] is [false]. The result is the list of value [y0, y1, ...].
    
    Example : [chain (split 0) (List.length >| (=) 1) l] returns the list [l]
              without its last element. *)
let rec chain f p x =
  if p x 
  then []
  else 
    let (answer, y) = f x in 
    answer :: (chain f p y)

(** {2 List of lists} *)
(** [swap x l] changes the hierarchy order in a list of lists. 
    For instance if [l2 = swap x l1], 
    the elements [List.nth j (List.nth i l1)] and [List.nth i (List.nth j l2)] are equal 
    if they both exist. If [List.nth j (List.nth i l1)] does not exist, then 
    [List.nth i (List.nth j l2)] is equal to [x]. 
    In [l2], all lists are of the same length, which is the maximum length 
    of the lists in [l1]. [swap x (swap x l1)] is therefore the same as [l1]
    except that all lists have been completed to the same length by addition
    of extra copies of [x] on their tails.

    [l1 = [[x00; x01; x02; ...];[x10; x11; x12; ...];[x20; x21; x22; ...]; ...]]

    [l2 = [[x00; x10; x20; ...];[x01; x11; x21; ...];[x02; x12; x22; ...]; ...]]
*)
let swap x = 
  chain (List.map (split x) >| unzip) (List.flatten >| (=) [])
