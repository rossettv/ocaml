#include "posix_stubs.h"

value caml_posix_fallocate(value vchannel, value dec, value len){
  CAMLparam3(vchannel,dec,len);
  struct channel * channel = Channel(vchannel);
  Lock(channel);
  posix_fallocate(channel->fd,Int_val(dec),Int_val(len));
  Unlock(channel);
  CAMLreturn(Val_unit);
}
